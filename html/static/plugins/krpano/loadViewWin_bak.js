//创建展示文本窗口
function createShowTextWin(hotspotId) {
	//先移除，再创建
	closeAllWin();
	console.log("textId"+hotspotId);

	//获取热点信息
	var spotInfo = getHotspotInfo("text", hotspotId);

	//整个窗体
	var showTextWin = document.createElement("div");
	showTextWin.id = "showTextWin";
	var clientWidth = document.body.clientWidth;
	//窗体的宽
	var showTextWinWidth = clientWidth * 0.6;
	showTextWin.setAttribute("style", "width: "+showTextWinWidth+"px;margin:0 auto;border-radius: 5px;overflow: hidden;z-index: 5000;background: none rgba(0, 0, 0, 0.4);opacity: 1;");
	var pleft = (clientWidth - showTextWinWidth)/2;
	showTextWin.style.position = "absolute";
	showTextWin.style.left = pleft + "px";
	showTextWin.style.top = "120px";

	//-----------------------------------------------
	//窗体顶部title
	var topTip = document.createElement("div");
	topTip.id = "topTip";
	topTip.setAttribute("style", "width: 100%;height: 45px;overflow: visible;z-index: 5001;pointer-events: none;background: none rgba(0, 0, 0, 0.4);text-size-adjust: none;opacity: 1;line-height: 45px;text-align: center;");
	var titleSpan = document.createElement("span");
	titleSpan.setAttribute("style", "color: #fff;font-family: microsoft yahei;font-weight: bold;margin-left: 10px;font-size: 15px;");
	titleSpan.innerHTML = spotInfo.tooltip;
	topTip.appendChild(titleSpan);
	//将窗体顶部title放进父级窗体
	showTextWin.appendChild(topTip);

	//-----------------------------------------------
	//外层div
	var outerLayerDiv = document.createElement("div");
	outerLayerDiv.id = "outerLayerDiv";
	outerLayerDiv.setAttribute("style", "width: 100%;height: 450px;overflow: visible;opacity: 1;z-index: 5001;");
	//将装场景列表的div放进父级窗体
	showTextWin.appendChild(outerLayerDiv);

	//用于遮挡滚动条的div
	var hiddenScroll = document.createElement("div");
	hiddenScroll.setAttribute("style", "width: 92%;height: 450px;overflow: hidden;opacity: 1;z-index: 5001;margin-left: 15px;");
	outerLayerDiv.appendChild(hiddenScroll);

	//文本存放的div
	var textDiv = document.createElement("div");
	textDiv.id = "textDiv";
	textDiv.setAttribute("style", "width: 106%;height: 450px;overflow: auto;opacity: 1;z-index: 5001;margin: 0 auto;");
	textDiv.innerHTML = spotInfo.content;
	hiddenScroll.appendChild(textDiv);

    //-----------------------------------------------
	//创建底部装按钮div
	var footBtn = document.createElement("div");
	footBtn.id = "footBtn";
	footBtn.setAttribute("style", "width: 100%;height: 50px;;opacity: 1;z-index: 5001;text-align: center;line-height: 50px;");

	//创建取消按钮
	var cancelBtn = document.createElement("button");
	cancelBtn.id = "cancelBtn";
	cancelBtn.innerText = "关闭";
	cancelBtn.setAttribute("style", "width: 10%;height: 30px;color: #FFF;background-color: #DE0101;border: none;border-radius: 5px;font-weight: bold;");
	cancelBtn.addEventListener("click", function() {
    closeAllViewWin();
	});
	//将取消按钮放入取消按钮div
    footBtn.appendChild(cancelBtn);
	//将装有底部按钮的div放进父级窗体
    showTextWin.appendChild(footBtn);
	//-----------------------------------------------
	//将窗体放入页面
	var body = document.body;
	body.appendChild(showTextWin);
}

//关闭所有窗口
function closeAllViewWin() {
	var dom1 = document.getElementById("showTextWin");
	if(null != dom1) {
		dom1.remove();
	}

}
