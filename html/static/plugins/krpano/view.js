//获取文本/标签热点信息
function getHotspotInfo(type, id) {
  var krpano = document.getElementById("view_pano");
	var reqUrl = "";
	var hotspotData = null;
	if("text" == type) {
		reqUrl = "/pano/texthotspot?id=" + id;
	} else if("tags" == type) {
		reqUrl = "/pano/taghotspot?id=" + id;
	} else {
		krpano.call("error_info_show(信息获取异常，请联系管理员！)");
		return;
	}

  httpService({
    url : reqUrl,
    method : 'GET',
    callback : (data) => {
      if (data && data.code == 200) {
        hotspotData = data.result;
      } else {
        krpano.call("error_info_show(信息获取异常，请联系管理员！)");
        return;
      }
    }
  });
	return hotspotData;
}
//ios，Android操作体统
var _userAgent = navigator.userAgent, app = navigator.appVersion;
var isAndroid = _userAgent.indexOf('Android') > -1 || _userAgent.indexOf('Linux') > -1; //g
// var isIOS = !!_userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端

//监听是否进入vr模式
var oldVRState=false;
function addVrListener(newVRState){
  if(oldVRState===newVRState){
    return;
  }
  oldVRState=newVRState;
  if(isAndroid){
    window.appH5Utils.vrState(oldVRState);
  }else{
    window.webkit.messageHandlers.vrState.postMessage({oldVRState:oldVRState});
  }
}

