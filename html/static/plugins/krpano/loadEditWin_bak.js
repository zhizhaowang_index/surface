var linkWinParam = {'winTitle': '请编辑链接信息', 'inpTitle': '链接标题', 'inpPlaceholder':'请输入链接标题', 'areaTitle': '链接URL', 'areaPlaceholder': '请输入链接URL'}
var textWinParam = {'winTitle': '请编辑文本信息', 'inpTitle': '文本标题', 'inpPlaceholder':'请输入文本标题', 'areaTitle': '文本内容', 'areaPlaceholder': '请输入文本内容'}
var tagWinParam = {'winTitle': '请编辑标签信息', 'inpTitle': '标签标题', 'inpPlaceholder':'请输入标签标题', 'areaTitle': '标签内容', 'areaPlaceholder': '请输入标签内容'}

function getId() {
    var len = len || 25;
    var $chars = 'abcdefghijklmnopqrstuvwxyz123456789';
    var maxPos = $chars.length;
    var uuid = '';
    for (var i = 0; i < len; i++) {
        uuid += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return uuid;
}

//创建热点样式选择框
function createHotspotStyleWin(winTitle, hotspotType, sceneId) {
	//先移除再创建
	closeAllWin();

	//整个窗体
	var hotspotStyleWin = document.createElement("div");
	hotspotStyleWin.id = "hotspotStyleWin";
	//窗体的宽
	var matterWinWidth = 400;
	hotspotStyleWin.setAttribute("style", "width: "+matterWinWidth+"px;margin:0 auto;border-radius: 10px;overflow: hidden;z-index: 5000;background: none rgba(0, 0, 0, 0.4);opacity: 1;");
	var clientWidth = document.body.clientWidth;
	var pleft = (clientWidth - matterWinWidth)/2;
	hotspotStyleWin.style.position = "absolute";
	hotspotStyleWin.style.left = pleft + "px";
	hotspotStyleWin.style.top = "120px";

//-----------------------------------------------
	//窗体顶部title
	var topTip = document.createElement("div");
	topTip.id = "topTip";
	topTip.setAttribute("style", "width: 100%;height: 45px;overflow: visible;z-index: 5001;pointer-events: none;background: none rgba(0, 0, 0, 0.4);text-size-adjust: none;opacity: 1;line-height: 45px;");
	var titleSpan = document.createElement("span");
	titleSpan.setAttribute("style", "color: #fff;font-family: microsoft yahei;font-weight: bold;margin-left: 10px;font-size: 15px;");
	titleSpan.innerHTML = winTitle;
	topTip.appendChild(titleSpan);
	//将窗体顶部title放进父级窗体
	hotspotStyleWin.appendChild(topTip);

//-----------------------------------------------
	//创建一个展示素材的div
	var matterDiv = document.createElement("div");
	matterDiv.id = "matterDiv";
	matterDiv.setAttribute("style", "width: 100%;height: 450px;overflow: visible;opacity: 1;z-index: 5001;");
	//将装素材的div放进父级窗体
	hotspotStyleWin.appendChild(matterDiv);
	//用于遮挡滚动条的div
	var hiddenScroll = document.createElement("div");
	hiddenScroll.setAttribute("style", "width: 92%;height: 450px;overflow: hidden;opacity: 1;z-index: 5001;margin-left: 15px;");
	matterDiv.appendChild(hiddenScroll);
	//素材内容div
	var contentDiv = document.createElement("div");
	contentDiv.id = "contentDiv";
	contentDiv.setAttribute("style", "width: 105%;height: 450px;overflow: auto;opacity: 1;z-index: 5001;margin: 0 auto;");
	//将素材内容div放进展示素材div
	hiddenScroll.appendChild(contentDiv);

  var krpano = document.getElementById("set_pano");
	//将单个素材放进素材内容div
	if(hotspotType == "hotspot") {
		//场景跳转热点样式----------
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "向前一", "front.png", 0, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "向前二", "front1.png", 168, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "向左", "left.png", -56, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "向右", "right.png", -112, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "左上", "leftup.png", -168, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "右上", "rightup.png", -224, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "定位一", "down.png", 336, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "定位二", "down1.png", 56, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "标记一", "circle1.png", -280, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "标记二", "circle4.png", -336, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "标记三", "circle2.png", 224, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "标记四", "circle3.png", 112, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "航拍一", "helicopter.png", 392, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "航拍二", "helicopter1.png", 280, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "点击", "finger.png", -392, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "查看", "magnifier.png", 504, 0));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "详情", "plus.png", 448, 0));
		//场景跳转热点样式----------
	} else if(hotspotType == "ring") {
    krpano.call("error_info_show(环物功能暂未开放！)");
    return;
		//环物热点样式----------
		/*contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式一", "0|288|56|48", 0, 336));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式二", "56|288|56|48", -56, 336));*/
		//环物热点样式----------
	} else if(hotspotType == "photo") {
    krpano.call("error_info_show(相册功能暂未开放！)");
    return;
		//相册热点样式----------
		/*contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式一", "0|192|56|48", 0, -192));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式二", "56|192|56|48", -56, -192));*/
		//相册热点样式----------
	} else if(hotspotType == "video") {
		//视频热点样式----------
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式一", "0|240|56|48", 0, -240));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式二", "56|240|56|48", -56, -240));
		//视频热点样式----------
	} else if(hotspotType == "link") {
    krpano.call("error_info_show(链接功能暂未开放！)");
    return;
		//链接热点样式----------
    /*
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式一", "0|48|56|48", 0, -48));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式二", "56|48|56|48", -56, -48));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式三", "112|48|56|48", -112, -48));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式四", "168|48|56|48", -168, -48));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式五", "224|48|56|48", -224, -48));*/
		//链接热点样式----------
	} else if(hotspotType == "text") {
    krpano.call("error_info_show(文本功能暂未开放！)");
    return;
		//文本热点样式----------
    /*
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式一", "0|96|56|48", 0, -96));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式二", "56|96|56|48", -56, -96));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式三", "112|96|56|48", -112, -96));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式四", "168|96|56|48", -168, -96));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式五", "224|96|56|48", -224, -96));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "样式六", "280|96|56|48", -280, -96));*/
		//文本热点样式----------
	} else if(hotspotType == "tags") {
    krpano.call("error_info_show(标签功能暂未开放！)");
    return;
		//标签热点样式----------
		/*contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "标签", "0|480|56|48", 0, 144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "红包", "56|480|56|48", -56, 144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "代金券", "112|480|56|48", -112, 144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "QQ", "168|480|56|48", -168, 144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "微博", "224|480|56|48", -224, 144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "微信", "280|480|56|48", -280, 144));*/
		//标签热点样式----------
	} else if(hotspotType == "snow") {
		//特效样式----------
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "小雨", "rain", 0, -144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "中雨", "heavyrain", -56, -144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "大雨", "bigrain", -112, -144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "小雪", "defaultsnow", -168, -144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "中雪", "snowballs", -280, -144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "大雪", "snowflakes", -224, -144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "银色星星", "silverstars", -336, -144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "金色星星", "goldenstars", -392, -144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "爱心", "hearts", -448, -144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "笑脸", "smileys", -504, -144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "钞票", "money", -560, -144));
		contentDiv.appendChild(createHotspotLabel(hotspotType, sceneId, "无特效", "nosnow", -616, -144));
		//特效样式----------
	}

//-----------------------------------------------
	//创建底部关闭按钮div
	var footBtnDiv = document.createElement("div");
	footBtnDiv.id = "footBtnDiv";
	footBtnDiv.setAttribute("style", "width: 100%;height: 50px;overflow: visible;opacity: 1;z-index: 5001;text-align: center;line-height: 50px;");

	//创建关闭按钮
	var closeBtn = document.createElement("button");
	closeBtn.id = "closeBtn";
	closeBtn.innerText = "关闭";
	closeBtn.setAttribute("style", "width: 20%;height: 30px;color: #FFF;font-weight: bold;background-color: #DE0101;border: none;border-radius: 5px;");
	closeBtn.addEventListener("click", function() {
		closeAllWin();
	});
	//将关闭按钮放入关闭按钮div
	footBtnDiv.appendChild(closeBtn);
	//将装有底部按钮的div放进父级窗体
	hotspotStyleWin.appendChild(footBtnDiv);

//-----------------------------------------------
	//将窗体放入页面
	var body = document.body;
	body.appendChild(hotspotStyleWin);
}
//创建热点样式及绑定事件的方法
var createHotspotLabel = function(hotspotType, sceneId, p_title, p_value, p_x, p_y) {
  var krpano = document.getElementById("set_pano");
	//创建单个素材div
	this.createOneLabel = function() {
		var hotspotLabelDiv = document.createElement("div");
		hotspotLabelDiv.setAttribute("style", "width: 70px;height: 80px;float: left;margin: 11px;cursor: pointer;");

		var imgDiv = document.createElement("div");
		imgDiv.setAttribute("style", "width: 56px;height: 48px;background-image: url('/static/plugins/krpano/skin/skin_img/edit.png');background-position: " + p_x + "px " + p_y + "px;margin: 0px auto;");

		var labelDiv = document.createElement("div");
		labelDiv.setAttribute("style", "width: 100%;height: 30px;border: none;border-radius: 20px;background-color: #FFFFFF;text-align: center;line-height: 28px;");

		var labelSpan = document.createElement("span");
		labelSpan.innerHTML = p_title;
		labelSpan.setAttribute("style", "color: #000000;font-family: microsoft yahei;font-weight: bold;font-size: 15px;");
		labelDiv.appendChild(labelSpan);

		hotspotLabelDiv.appendChild(imgDiv);
		hotspotLabelDiv.appendChild(labelDiv);
		bandEventFun(hotspotLabelDiv, hotspotType, sceneId, p_value);
		return hotspotLabelDiv;
	}

	this.bandEventFun = function(dom, hotspotType, sceneId, p_value) {
		dom.addEventListener("click", function() {
			if(hotspotType == "hotspot") {
				closeFun("hotspotStyleWin");
				var addParam = {"hotspotType": hotspotType, "sceneId": sceneId, "style": p_value}
				createSceneWin('add', addParam, null);
			} else if(hotspotType == "ring") {
				closeFun("hotspotStyleWin");
				var winParam = {'title': '选择环物', 'isMore': false}
				var addParam = {'hotspotType': hotspotType, 'sceneId': sceneId, 'style': p_value}
				createChoseWin('add', winParam, addParam, null);
			} else if(hotspotType == "photo") {
				closeFun("hotspotStyleWin");
				var winParam = {'title': '选择图片', 'isMore': true}
				var addParam = {'hotspotType': hotspotType, 'sceneId': sceneId, 'style': p_value}
				createChoseWin('add', winParam, addParam, null);
			} else if(hotspotType == "video") {

			} else if(hotspotType == "link") {
				closeFun("hotspotStyleWin");
				var addParam = {'hotspotType': hotspotType, 'sceneId': sceneId, 'style': p_value}
				createInputWin("add", linkWinParam, addParam, null);
			} else if(hotspotType == "text") {
				closeFun("hotspotStyleWin");
				var addParam = {'hotspotType': hotspotType, 'sceneId': sceneId, 'style': p_value}
				createInputWin("add", textWinParam, addParam, null);
			} else if(hotspotType == "tags") {
				closeFun("hotspotStyleWin");
				var addParam = {'hotspotType': hotspotType, 'sceneId': sceneId, 'style': p_value}
				createInputWin("add", tagWinParam, addParam, null);
			} else if(hotspotType == "snow") {
				saveSnow(p_value);

				krpano.call(p_value+"();");
			}
		});
		var childList = dom.childNodes;
		var second = childList[1];
		dom.addEventListener("mouseover", function() {
			second.style.backgroundColor = "#33b9f9";
		});
		dom.addEventListener("mouseout", function() {
			second.style.backgroundColor = "#FFFFFF";
		});
	}
	return createOneLabel();
}


//素材选择窗口
//isMore -- 素材是否多选
function createChoseWin(opera, winParam, addParam, editParam) {
  var krpano = document.getElementById("set_pano");
	//先移除，再创建
	closeAllWin();

	//整个窗体
	var matterWin = document.createElement("div");
	matterWin.id = "matterWin";
	//窗体的宽
	var matterWinWidth = 440;
	matterWin.setAttribute("style", "width: "+matterWinWidth+"px;margin:0 auto;border-radius: 10px;overflow: hidden;z-index: 5000;background: none rgba(0, 0, 0, 0.4);opacity: 1;");
	var clientWidth = document.body.clientWidth;
	var pleft = (clientWidth - matterWinWidth)/2;
	matterWin.style.position = "absolute";
	matterWin.style.left = pleft + "px";
	matterWin.style.top = "120px";

//-----------------------------------------------
	//窗体顶部title
	var topTip = document.createElement("div");
	topTip.id = "topTip";
	topTip.setAttribute("style", "width: 100%;height: 45px;overflow: visible;z-index: 5001;pointer-events: none;background: none rgba(0, 0, 0, 0.4);text-size-adjust: none;opacity: 1;line-height: 45px;");
	var titleSpan = document.createElement("span");
	titleSpan.setAttribute("style", "color: #fff;font-family: microsoft yahei;font-weight: bold;margin-left: 10px;font-size: 15px;");
	titleSpan.innerHTML = winParam.title;
	topTip.appendChild(titleSpan);
	//将窗体顶部title放进父级窗体
	matterWin.appendChild(topTip);

//-----------------------------------------------
	//窗体中上方的装搜索块的div
	var middleSearchDiv = document.createElement("div");
	middleSearchDiv.id = "middleSearchDiv";
	middleSearchDiv.setAttribute("style", "width: 100%;height: 50px;overflow: visible;opacity: 1;z-index: 5001;");
	//窗体中上方的输入框div
	var middleSearchInpDiv = document.createElement("div");
	middleSearchInpDiv.id = "middleSearchInpDiv";
	middleSearchInpDiv.setAttribute("style", "width: 74%;height: 50px;float: left;overflow: visible;opacity: 1;z-index: 5001;text-align: center;line-height: 47px;");
	//窗体中上方的输入框
	var middleSearchInput = document.createElement("input");
	middleSearchInput.id="middleSearchInput";
	middleSearchInput.setAttribute("style", "width: 82%;height: 30px;border-radius: 5px;");
	middleSearchInput.placeholder = " 输入搜索关键字";
	//把输入框放入输入框div中
	middleSearchInpDiv.appendChild(middleSearchInput);

	//窗体中上方的搜索按钮div
	var middleSearchBtnDiv = document.createElement("div");
	middleSearchBtnDiv.id = "middleSearchBtnDiv";
	middleSearchBtnDiv.setAttribute("style", "width: 20%;height: 50px;float: left;overflow: visible;opacity: 1;z-index: 5001;text-align: center;line-height: 50px;");
	//窗体中上方的搜索按钮
	var middleSearchBtn = document.createElement("button");
	middleSearchBtn.id = "middleSearchBtn";
	middleSearchBtn.innerText = "搜索";
	middleSearchBtn.setAttribute("style", "width: 100%;height: 35px;color: #FFF;background-color: #b36d16;border: none;border-radius: 5px;");
	//把搜索按钮放入搜索按钮div中
	middleSearchBtnDiv.appendChild(middleSearchBtn);

	//将输入框div放入div
	middleSearchDiv.appendChild(middleSearchInpDiv);
	//将搜索按钮div放入div
	middleSearchDiv.appendChild(middleSearchBtnDiv);
	//将装有搜索框的div放进父级窗体
	matterWin.appendChild(middleSearchDiv);

//-----------------------------------------------
	//窗体中上方的装输入框标题和输入框的div
	var middleDiv = document.createElement("div");
	middleDiv.id = "middleDiv";
	middleDiv.setAttribute("style", "width: 100%;height: 75px;overflow: visible;opacity: 1;z-index: 5001;");
	//创建输入框标题div
	var middleTabDiv = document.createElement("div");
	middleTabDiv.id = "middleTabDiv";
	middleTabDiv.setAttribute("style", "width: 88%;height: 30px;line-height: 30px;overflow: visible;opacity: 1;z-index: 5001;margin: 0 auto;");
	var middleTabSpan = document.createElement("span");
	middleTabSpan.innerHTML = "标&nbsp;题";
	middleTabSpan.setAttribute("style", "font-family: microsoft yahei;font-weight: bold;color: #fff;");
	middleTabDiv.appendChild(middleTabSpan);
	middleDiv.appendChild(middleTabDiv);
	//创建输入框div
	var middleInpDiv = document.createElement("div");
	middleInpDiv.id = "middleInpDiv";
	middleInpDiv.setAttribute("style", "width: 88%;height: 40px;overflow: visible;opacity: 1;z-index: 5001;margin: 0 auto;");
	middleDiv.appendChild(middleInpDiv);
	//创建输入框
	var middleInp = document.createElement("input");
	middleInp.id = "middleInp";
	middleInp.setAttribute("style", "width: 99%;height: 30px;border-radius: 5px;");
	middleInp.placeholder = " 请填写标题信息";
	middleInpDiv.appendChild(middleInp);
	//将装有输入框标题和输入框的div放进父级窗体
	matterWin.appendChild(middleDiv);

//-----------------------------------------------
	//创建一个展示素材的div
	var matterDiv = document.createElement("div");
	matterDiv.id = "matterDiv";
	matterDiv.setAttribute("style", "width: 100%;height: 400px;overflow: visible;opacity: 1;z-index: 5001;");
	//将装素材的div放进父级窗体
	matterWin.appendChild(matterDiv);
	//用于遮挡滚动条的div
	var hiddenScroll = document.createElement("div");
	hiddenScroll.setAttribute("style", "width: 92%;height: 400px;overflow: hidden;opacity: 1;z-index: 5001;margin-left: 15px;");
	matterDiv.appendChild(hiddenScroll);
	//素材内容div
	var contentDiv = document.createElement("div");
	contentDiv.id = "contentDiv";
	contentDiv.setAttribute("style", "width: 106%;height: 400px;overflow: auto;opacity: 1;z-index: 5001;margin: 0 auto;");
	//将素材内容div放进展示素材div
	hiddenScroll.appendChild(contentDiv);
	//将单个素材放进素材内容div
	if("add" == opera) {
		var picData = getPicList(addParam.hotspotType, "");
		if(null != picData && picData.length != 0) {
			for(var i=0;i<picData.length;i++) {
				contentDiv.appendChild(createImgDiv(winParam.isMore, picData[i]));
			}
		} else {
      return;
    }
	}

//-----------------------------------------------
	//创建底部装按钮div
	var footBtn = document.createElement("div");
	footBtn.id = "footBtn";
	footBtn.setAttribute("style", "width: 100%;height: 50px;overflow: visible;opacity: 1;z-index: 5001;");
	//确定按钮div
	var sureBtnDiv = document.createElement("div");
	sureBtnDiv.setAttribute("style", "width: 50%;height: 50px;float: left;text-align: center;line-height: 50px;");
	//将确定按钮div放进底部按钮div中
	footBtn.appendChild(sureBtnDiv);
	//创建确定按钮
	var sureBtn = document.createElement("button");
	sureBtn.id = "sureBtn";
	sureBtn.innerText = "确定";
	sureBtn.setAttribute("style", "width: 40%;height: 30px;color: #FFF;background-color: #2E6DA4;border: none;border-radius: 5px;");
	sureBtn.addEventListener("click", function() {
		var tooltip = document.getElementById("middleInp").value;
		if("" == tooltip.trim() || tooltip.trim().length == 0) {
			krpano.call("error_info_show(请填写标题信息！)");
			return;
		}

		var valDom = document.getElementById("contentDiv").childNodes;
		var arrs = new Array();
		for(var i=0;i<valDom.length;i++) {
			var cvalue = valDom[i].getAttribute("checked");
			if(cvalue == "true") {
				arrs.push(valDom[i].getAttribute("did"));
			}
		}
		if(arrs.length == 0) {
			krpano.call("error_info_show(请至少选中一个！)");
			return;
		}

		var hotspotId = addParam.hotspotType +"_"+getId();
		if('ring' == addParam.hotspotType) {
			addRingHotspot(hotspotId, addParam.sceneId, addParam.style, tooltip, arrs.toString());
		} else if('photo' == addParam.hotspotType) {
			addPicHotspot(hotspotId, addParam.sceneId, addParam.style, tooltip, arrs.toString());
		} else {
			krpano.call("error_info_show(参数类型错误！)");
			return;
		}
		krpano.call("addStaticHotspot(" + hotspotId + "," + addParam.style + "," + tooltip + ")");
		closeAllWin();
	});
	//将确定按钮放入确定按钮div
	sureBtnDiv.appendChild(sureBtn);

	//取消按钮div
	var cancelBtnDiv = document.createElement("div");
	cancelBtnDiv.setAttribute("style", "width: 50%;height: 50px;float: left;text-align: center;line-height: 50px;");
	//将取消按钮div放进底部按钮div中
	footBtn.appendChild(cancelBtnDiv);
	//创建取消按钮
	var cancelBtn = document.createElement("button");
	cancelBtn.id = "cancelBtn";
	cancelBtn.innerText = "取消";
	cancelBtn.setAttribute("style", "width: 40%;height: 30px;color: #FFF;background-color: #DE0101;border: none;border-radius: 5px;");
	cancelBtn.addEventListener("click", function() {
		closeAllWin();
	});
	//将取消按钮放入取消按钮div
	cancelBtnDiv.appendChild(cancelBtn);
	//将装有底部按钮的div放进父级窗体
	matterWin.appendChild(footBtn);

	//给搜索按钮绑定事件
	middleSearchBtn.addEventListener("click", function() {
		var keyword = document.getElementById("middleSearchInput").value;
		var list = getPicList(addParam.hotspotType, keyword);
		if(null != list && list.length != 0) {
			contentDiv.innerHTML = "";
			for(var i=0;i<list.length;i++) {
				contentDiv.appendChild(createImgDiv(winParam.isMore, list[i]));
			}
		}
	});
//-----------------------------------------------
	//将窗体放入页面
	var body = document.body;
	body.appendChild(matterWin);
}

//创建单个素材方法
//ismore是否支持多选 true --表示可选择多个 false--表示只能选中一个
var createImgDiv = function(isMore, picDom) {
	this.divDom = null;
	//创建单个素材div
	this.createMethod = function() {
		var imgContentDiv = document.createElement("div");
        imgContentDiv.setAttribute("checked", "false");
        imgContentDiv.setAttribute("did", picDom.id);
        imgContentDiv.setAttribute("style", "width: 182px;height: 80px;border-width: 5px;border-style: solid;border-color: #6b6b6b;border-radius: 5px;float: left;margin: 10px 0px 0px 10px;");
        imgContentDiv.addEventListener("click", function() {
			chooseFun(imgContentDiv, isMore);
		});

        var imgDiv = document.createElement("div");
        imgDiv.setAttribute("style", "width: 100%;height: 80px;margin: auto;position: relative;cursor:pointer;");

		var img = document.createElement("img");
		img.src = picDom.url;
		img.width = "182";
		img.height = "80";
        imgDiv.appendChild(img);
        imgContentDiv.appendChild(imgDiv);

        var textDiv = document.createElement("div");
        textDiv.setAttribute("style", "width: 100%;height: 30px;background:none rgba(0, 0, 0, 0.2);text-align: center;line-height: 30px;position: absolute;bottom: 0px;");
        var textSpan = document.createElement("span");
        textSpan.setAttribute("style", "color: #fff;font-family: microsoft yahei;font-weight: bold;font-size: 15px;");
        textSpan.innerHTML = picDom.name;
        textDiv.appendChild(textSpan);
        imgDiv.appendChild(textDiv);
		return imgContentDiv;
	}

	//选中效果
	//true --表示可选择多个 false --表示只能选中一个
	this.chooseFun = function(obj, boo) {
		if(obj.getAttribute("checked") == "true") {
			obj.style.borderColor = "#6b6b6b";
			obj.setAttribute("checked", "false");
			if(!boo) {
				this.divDom = null;
			}
		} else {
			obj.style.borderColor = "#6EA5C8";
			obj.setAttribute("checked", "true");
			if(!boo) {
				if(null != divDom) {
					this.divDom.style.borderColor = "#6b6b6b";
					this.divDom.setAttribute("checked", "false");
				}
				this.divDom = obj;
			}
		}
	}

	return createMethod();
}

//文本-标签-链接热点输入窗口
function createInputWin(opera, winParam, addParam, editParam) {
  var krpano = document.getElementById("set_pano");
	//先移除，再创建
	closeAllWin();

	//修改时先获取原信息
	var info = null;
	if("edit" == opera) {
		info = getOriginalHotspotInfo(editParam.hotspotType, editParam.hotspotId);
	}

	//整个窗体
	var inputWin = document.createElement("div");
	inputWin.id = "inputWin";
	//窗体的宽
	var inputWinWidth = 440;
	inputWin.setAttribute("style", "width: "+inputWinWidth+"px;margin:0 auto;border-radius: 10px;overflow: hidden;z-index: 5000;background: none rgba(0, 0, 0, 0.4);opacity: 1;");
	var clientWidth = document.body.clientWidth;
	var pleft = (clientWidth - inputWinWidth)/2;
	inputWin.style.position = "absolute";
	inputWin.style.left = pleft + "px";
	inputWin.style.top = "120px";

//-----------------------------------------------
	//窗体顶部title
	var topTip = document.createElement("div");
	topTip.id = "topTip";
	topTip.setAttribute("style", "width: 100%;height: 45px;overflow: visible;z-index: 5001;pointer-events: none;background: none rgba(0, 0, 0, 0.4);text-size-adjust: none;opacity: 1;line-height: 45px;");
	var titleSpan = document.createElement("span");
	titleSpan.setAttribute("style", "color: #fff;font-family: microsoft yahei;font-weight: bold;margin-left: 10px;font-size: 15px;");
	titleSpan.innerHTML = winParam.winTitle;
	topTip.appendChild(titleSpan);
	//将窗体顶部title放进父级窗体
	inputWin.appendChild(topTip);

//-----------------------------------------------
	//窗体中上方的装输入框标题和输入框的div
	var middleDiv = document.createElement("div");
	middleDiv.id = "middleDiv";
	middleDiv.setAttribute("style", "width: 100%;height: 75px;overflow: visible;opacity: 1;z-index: 5001;");
	//创建输入框标题div
	var middleTabDiv = document.createElement("div");
	middleTabDiv.id = "middleTabDiv";
	middleTabDiv.setAttribute("style", "width: 88%;height: 30px;line-height: 30px;overflow: visible;opacity: 1;z-index: 5001;margin: 0 auto;");
	var middleTabSpan = document.createElement("span");
	middleTabSpan.innerHTML = winParam.inpTitle;
	middleTabSpan.setAttribute("style", "font-family: microsoft yahei;font-weight: bold;color: #fff;");
	middleTabDiv.appendChild(middleTabSpan);
	middleDiv.appendChild(middleTabDiv);
	//创建输入框div
	var middleInpDiv = document.createElement("div");
	middleInpDiv.id = "middleInpDiv";
	middleInpDiv.setAttribute("style", "width: 88%;height: 40px;overflow: visible;opacity: 1;z-index: 5001;margin: 0 auto;");
	middleDiv.appendChild(middleInpDiv);
	//创建输入框
	var middleInp = document.createElement("input");
	middleInp.id = "middleInp";
	middleInp.setAttribute("style", "width: 99%;height: 30px;border-radius: 5px;");
	null == info ? middleInp.placeholder = " " + winParam.inpPlaceholder : middleInp.value = info.tooltip;
	middleInpDiv.appendChild(middleInp);
	//将装有输入框标题和输入框的div放进父级窗体
	inputWin.appendChild(middleDiv);
//-----------------------------------------------
	//窗体中间的装文本域标题和文本域的div
	var middleAreaDiv = document.createElement("div");
	middleAreaDiv.id = "middleAreaDiv";
	middleAreaDiv.setAttribute("style", "width: 100%;height: 200px;overflow: visible;opacity: 1;z-index: 5001;");
	//创建文本域标题div
	var middleAreaTabDiv = document.createElement("div");
	middleAreaTabDiv.id = "middleAreaTabDiv";
	middleAreaTabDiv.setAttribute("style", "width: 88%;height: 30px;line-height: 30px;overflow: visible;opacity: 1;z-index: 5001;margin: 0 auto;");
	var middleAreaTabSpan = document.createElement("span");
	middleAreaTabSpan.innerHTML = winParam.areaTitle;
	middleAreaTabSpan.setAttribute("style", "font-family: microsoft yahei;font-weight: bold;color: #fff;");
	middleAreaTabDiv.appendChild(middleAreaTabSpan);
	middleAreaDiv.appendChild(middleAreaTabDiv);

	//创建文本域div
	var middleTextAreaDiv = document.createElement("div");
	middleTextAreaDiv.id = "middleTextAreaDiv";
	middleTextAreaDiv.setAttribute("style", "width: 88%;height: 40px;overflow: visible;opacity: 1;z-index: 5001;margin: 0 auto;");
	//创建文本域
	var middleTextArea = document.createElement("textarea");
	middleTextArea.id = "middleTextArea";
	middleTextArea.placeholder = winParam.areaPlaceholder;
	if("edit" == opera) {
		if("link" == editParam.hotspotType) {
			middleTextArea.value = info.link;
		} else if("text" == editParam.hotspotType) {
			middleTextArea.value = info.content;
		} else if("tags" == editParam.hotspotType) {
			middleTextArea.value = info.tag;
		}
	}
	middleTextArea.setAttribute("style", "width: 99%;height: 150px;border-radius: 5px;");

	middleTextAreaDiv.appendChild(middleTextArea);
	middleAreaDiv.appendChild(middleTextAreaDiv);
	//将装有文本域标题和文本域的div放进父级窗体
	inputWin.appendChild(middleAreaDiv);
//-----------------------------------------------
	//创建底部装按钮div
	var footBtn = document.createElement("div");
	footBtn.id = "footBtn";
	footBtn.setAttribute("style", "width: 100%;height: 50px;overflow: visible;opacity: 1;z-index: 5001;");
	//确定按钮div
	var sureBtnDiv = document.createElement("div");
	sureBtnDiv.setAttribute("style", "width: 50%;height: 50px;float: left;text-align: center;line-height: 50px;");
	//将确定按钮div放进底部按钮div中
	footBtn.appendChild(sureBtnDiv);
	//创建确定按钮
	var sureBtn = document.createElement("button");
	sureBtn.id = "sureBtn";
	sureBtn.innerText = "确定";
	sureBtn.setAttribute("style", "width: 40%;height: 30px;color: #FFF;background-color: #2E6DA4;border: none;border-radius: 5px;");
	sureBtn.addEventListener("click", function() {
		var val1 = document.getElementById("middleInp").value.trim();
		var val2 = document.getElementById("middleTextArea").value.trim();
		if(null == val1 || val1.length == 0 || null == val2 || val2.length == 0) {
			krpano.call("error_info_show(参数不能为空！)");
			return;
		}

		if("add" == opera) {
			var hotspotId = addParam.hotspotType +"_"+getId();
			if("link" == addParam.hotspotType) {
				var reg = /^http(s|):\/\/.*/;
				if(!reg.test(val2)) {
					val2 = "http://" + val2;
				}
				addLinkHotspot(hotspotId, addParam.sceneId, addParam.style, val1, val2);
			} else if("text" == addParam.hotspotType) {
				addTextHotspot(hotspotId, addParam.sceneId, addParam.style, val1, val2);
			} else if("tags" == addParam.hotspotType) {
				addTagHotspot(hotspotId, addParam.sceneId, addParam.style, val1, val2);
			}
			krpano.call("addStaticHotspot(" + hotspotId + "," + addParam.style + "," + val1 + ")");
			closeAllWin();
		} else if("edit" == opera) {
			if("link" == editParam.hotspotType) {
				var pattern = /^http(s|):\/\/.*/;
				if(!pattern.test(val2)) {
					val2 = "http://" + val2;
				}
				updateLinkHotspot(info.id, val1, val2);
			} else if("text" == editParam.hotspotType) {
				updateTextHotspot(info.id, val1, val2);
			} else if("tags" == editParam.hotspotType) {
				updateTagHotspot(info.id, val1, val2);
			}
			krpano.call("set_tooltip(" + info.id + "," + val1 + ")");
			closeAllWin();
		} else {
			krpano.call("error_info_show(请求异常，请联系管理员！)");
			return;
		}
	});
	//将确定按钮放入确定按钮div
	sureBtnDiv.appendChild(sureBtn);

	//取消按钮div
	var cancelBtnDiv = document.createElement("div");
	cancelBtnDiv.setAttribute("style", "width: 50%;height: 50px;float: left;text-align: center;line-height: 50px;");
	//将取消按钮div放进底部按钮div中
	footBtn.appendChild(cancelBtnDiv);
	//创建取消按钮
	var cancelBtn = document.createElement("button");
	cancelBtn.id = "cancelBtn";
	cancelBtn.innerText = "取消";
	cancelBtn.setAttribute("style", "width: 40%;height: 30px;color: #FFF;background-color: #DE0101;border: none;border-radius: 5px;");
	cancelBtn.addEventListener("click", function() {
		closeAllWin();
	});
	//将取消按钮放入取消按钮div
	cancelBtnDiv.appendChild(cancelBtn);
	//将装有底部按钮的div放进父级窗体
	inputWin.appendChild(footBtn);

//-----------------------------------------------
	//将窗体放入页面
	var body = document.body;
	body.appendChild(inputWin);
}

//场景选择窗口
function createSceneWin(opera, addParam, edtiParam) {
	//先移除，再创建
	closeAllWin();

	//整个窗体
	var sceneWin = document.createElement("div");
	sceneWin.id = "sceneWin";
	//窗体的宽
	var sceneWinWidth = 440;
	sceneWin.setAttribute("style", "width: "+sceneWinWidth+"px;margin:0 auto;border-radius: 10px;overflow: hidden;z-index: 5000;background: none rgba(0, 0, 0, 0.4);opacity: 1;");
	var clientWidth = document.body.clientWidth;
	var pleft = (clientWidth - sceneWinWidth)/2;
	sceneWin.style.position = "absolute";
	sceneWin.style.left = pleft + "px";
	sceneWin.style.top = "120px";

//-----------------------------------------------
	//窗体顶部title
	var topTip = document.createElement("div");
	topTip.id = "topTip";
	topTip.setAttribute("style", "width: 100%;height: 45px;overflow: visible;z-index: 5001;pointer-events: none;background: none rgba(0, 0, 0, 0.4);text-size-adjust: none;opacity: 1;line-height: 45px;");
	var titleSpan = document.createElement("span");
	titleSpan.setAttribute("style", "color: #fff;font-family: microsoft yahei;font-weight: bold;margin-left: 10px;font-size: 15px;");
	titleSpan.innerHTML = "请选择跳转场景";
	topTip.appendChild(titleSpan);
	//将窗体顶部title放进父级窗体
	sceneWin.appendChild(topTip);

//-----------------------------------------------
	//场景列表存放div
	var sceneListDiv = document.createElement("div");
	sceneListDiv.id = "sceneListDiv";
	sceneListDiv.setAttribute("style", "width: 100%;height: 450px;overflow: visible;opacity: 1;z-index: 5001;");
	//将装场景列表的div放进父级窗体
	sceneWin.appendChild(sceneListDiv);

	//用于遮挡滚动条的div
	var hiddenScroll = document.createElement("div");
	hiddenScroll.setAttribute("style", "width: 92%;height: 450px;overflow: hidden;opacity: 1;z-index: 5001;margin-left: 15px;");
	sceneListDiv.appendChild(hiddenScroll);

	//场景列表滚动的div
	var sceneDiv = document.createElement("div");
    sceneDiv.id = "sceneDiv";
    sceneDiv.setAttribute("style", "width: 106%;height: 450px;overflow: auto;opacity: 1;z-index: 5001;margin: 0 auto;");
    hiddenScroll.appendChild(sceneDiv);

    //加载场景列表数据
    var sceneData = getSceneList(null != addParam ? addParam.sceneId : edtiParam.sceneId);
    if(null != sceneData && sceneData.length != 0) {
      for(var i=0;i<sceneData.length;i++) {
        sceneDiv.appendChild(createSceneDiv(opera, addParam, edtiParam, sceneData[i]));
      }
    } else {
      return;
    }

//-----------------------------------------------
	//创建底部装按钮div
	var footBtn = document.createElement("div");
	footBtn.id = "footBtn";
	footBtn.setAttribute("style", "width: 100%;height: 50px;;opacity: 1;z-index: 5001;text-align: center;line-height: 50px;");

	//创建取消按钮
	var cancelBtn = document.createElement("button");
	cancelBtn.id = "cancelBtn";
	cancelBtn.innerText = "关闭";
	cancelBtn.setAttribute("style", "width: 20%;height: 30px;color: #FFF;background-color: #DE0101;border: none;border-radius: 5px;font-weight: bold;");
	cancelBtn.addEventListener("click", function() {
        closeAllWin();
	});
	//将取消按钮放入取消按钮div
    footBtn.appendChild(cancelBtn);
	//将装有底部按钮的div放进父级窗体
	sceneWin.appendChild(footBtn);

//-----------------------------------------------
	//将窗体放入页面
	var body = document.body;
	body.appendChild(sceneWin);
}

//创建场景列表方法
var createSceneDiv = function(opera, addParam, edtiParam, sceneDom) {
  var krpano = document.getElementById("set_pano");
	//创建单个素材div
	this.createMethod = function() {
        var sceneContentDiv = document.createElement("div");
        sceneContentDiv.setAttribute("goSceneId", sceneDom.id);
        sceneContentDiv.setAttribute("goSceneName", sceneDom.sceneName);
        sceneContentDiv.setAttribute("style", "width: 91%;height: 80px;margin: 10px 17px;border-radius: 8px;background: none rgba(0, 0, 0, 0.2); opacity: 1;cursor: pointer;");

        this.chooseFun(sceneContentDiv);
        var sceneImgDiv = document.createElement("div");
        sceneImgDiv.setAttribute("style", "width: 19%;height: 70px;float: left;margin-top: 5px;margin-left: 5px;");
        var sceneImg = document.createElement("img");
        sceneImg.src = sceneDom.coverUrl;
        sceneImg.width = "70";
        sceneImg.height = "70";
        sceneImgDiv.appendChild(sceneImg);
        sceneContentDiv.appendChild(sceneImgDiv);

        var sceneTextDiv = document.createElement("div");
        sceneTextDiv.setAttribute("style", "width: 75%;height: 70px;float: left;margin-top: 5px;margin-left: 10px;text-align: center;line-height: 70px;");
        sceneContentDiv.appendChild(sceneTextDiv);

        var sceneTextSpan = document.createElement("span");
        sceneTextSpan.setAttribute("style", "color: #FFF;font-family: microsoft yahei;");
        sceneTextSpan.innerHTML = sceneDom.sceneName;
        sceneTextDiv.appendChild(sceneTextSpan);

        return sceneContentDiv;
	}

	//选中效果
	this.chooseFun = function(dom) {
        dom.addEventListener("mouseover", function () {
			dom.style.background = "none rgba(0, 0, 0, 0.3)";
        });

        dom.addEventListener("mouseout", function () {
			dom.style.background = "none rgba(0, 0, 0, 0.2)";
        });

        dom.addEventListener("click", function () {
			var goScene = this.getAttribute("goSceneId");
			var goSceneName = this.getAttribute("goSceneName");
			if("add" == opera) {
				var hotspotId = addParam.hotspotType +"_"+getId();
				addHotspot(hotspotId, addParam.sceneId, goScene, addParam.style, goSceneName);
				krpano.call("addDynamicHotspot(" + hotspotId + "," + addParam.style + "," + goSceneName + ")");
				closeAllWin();
		    } else if("edit" == opera) {
		    	var hotspotId = edtiParam.hotspotId;
				updateHotspot(hotspotId, goScene, goSceneName);
				krpano.call("set_tooltip(" + hotspotId + "," + goSceneName + ")");
				closeAllWin();
		    } else {
		    	krpano.call("error_info_show(请求异常，请联系管理员！)");
	 			return;
		    }
        });
	}

	return createMethod();
}

//修改热点
function editHotspot(hotspotId, sceneId) {
  var krpano = document.getElementById("set_pano");
	var strArr = hotspotId.split("_");
	var head = strArr[0];
	var editParam = {"hotspotId": hotspotId, "hotspotType": head, "sceneId": sceneId}
	if(head == "hotspot") {
		createSceneWin("edit", null, editParam);
	} else if(head == "ring") {
		krpano.call("error_info_show(暂不支持编辑，请删除后重建！)");
	} else if(head == "photo") {
		krpano.call("error_info_show(暂不支持编辑，请删除后重建！)");
	} else if(head == "video") {
		krpano.call("error_info_show(暂不支持编辑，请删除后重建！)");
	} else if(head == "link") {
		createInputWin("edit", linkWinParam, null, editParam);
	} else if(head == "text") {
		createInputWin("edit", textWinParam, null, editParam);
	} else if(head == "tags") {
		createInputWin("edit", tagWinParam, null, editParam);
	}
}

//关闭按钮绑定函数
function closeFun(winId) {
	var dom = document.getElementById(winId);
	if(null != dom) {
		dom.remove();
	}
}

//关闭所有窗口
function closeAllWin() {
	var dom1 = document.getElementById("hotspotStyleWin");
	if(null != dom1) {
		dom1.remove();
	}

	var dom2 = document.getElementById("matterWin");
	if(null != dom2) {
		dom2.remove();
	}

	var dom3 = document.getElementById("inputWin");
	if(null != dom3) {
		dom3.remove();
	}

	var dom4 = document.getElementById("sceneWin");
	if(null != dom4) {
		dom4.remove();
	}
}
