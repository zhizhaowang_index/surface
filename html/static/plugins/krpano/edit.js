// let prefix = 'http://test.zhizhaowang.cn:8882';
let prefix = 'http://www.zhizhaowang.cn:8088';

let imgUrl = 'http://cdn.zhizhaowang.cn/';
async function httpService(subConfig) {
  let url = prefix + subConfig.url;
  if (subConfig.params) {
    for (let key in subConfig.params) {
      url = url.replace('{' + key + '}', subConfig.params[key])
    }
  }
  let config = {
    url: url,
    method: subConfig.method,
  };
  if (subConfig.method === 'GET') {
    config['params'] = subConfig.data
  } else {
    config['data'] = subConfig.data
  }
  $.ajax({
    type: subConfig.method,
    url: url,
    contentType: "application/json;charset=UTF-8",
    data: JSON.stringify(config['data']),
    cache:false,
    async:false,
    dataType: "JSON",
    xhrFields: {
      withCredentials: true
    },
    success: function (data) {
      if (subConfig.callback != undefined) {
        subConfig.callback(data);
      }
    },
    error: function(data) {
      if (subConfig.callback != undefined) {
        subConfig.callback(data);
      }
    }
  });
}


let krpano,panoId;
function getData() {
  panoId = localStorage.getItem('panoId') || '';
  krpano = document.getElementById("set_pano");
}

//加载当前场景所有存在的热点
function loadExistingHotspot(sceneId) {
  getData();
  httpService({
    url : "/resource/pano/hotspot/list?sceneId=" + sceneId,
    method : 'GET',
    callback : (data) => {
      if (data && data.code == 200) {
        var result = data.result;

        //渲染当前场景场景切换热点
        var hLen = result.Hotspot.length;
        if(hLen != 0) {
          var hData = result.Hotspot;
          for(var i=0;i<hLen;i++) {
            krpano.call("loadSceneJumpHotspot(" + hData[i].id + "," + hData[i].style + "," + hData[i].tooltip + "," + hData[i].ath + "," + hData[i].atv + ")");
          }
        }

        //渲染当前场景环物热点
        var rLen = result.RingHotspot.length;
        if(rLen != 0) {
          var rData = result.RingHotspot;
          for(var i=0;i<rLen;i++) {
            krpano.call("loadStaticHotspot(" + rData[i].id + "," + rData[i].style + "," + rData[i].tooltip + "," + rData[i].ath + "," + rData[i].atv + ")");
          }
        }

        //渲染当前场景图片热点
        var pLen = result.PicHotspot.length;
        if(pLen != 0) {
          var pData = result.PicHotspot;
          for(var i=0;i<pLen;i++) {
            krpano.call("loadStaticHotspot(" + pData[i].id + "," + pData[i].style + "," + pData[i].tooltip + "," + pData[i].ath + "," + pData[i].atv + ")");
          }
        }

        //渲染当前场景链接热点
        var lLen = result.LinkHotspot.length;
        if(lLen != 0) {
          var lData = result.LinkHotspot;
          for(var i=0;i<lLen;i++) {
            krpano.call("loadStaticHotspot(" + lData[i].id + "," + lData[i].style + "," + lData[i].tooltip + "," + lData[i].ath + "," + lData[i].atv + ")");
          }
        }

        //渲染当前场景文本热点
        var tLen = result.TextHotspot.length;
        if(tLen != 0) {
          var tData = result.TextHotspot;
          for(var i=0;i<tLen;i++) {
            krpano.call("loadStaticHotspot(" + tData[i].id + "," + tData[i].style + "," + tData[i].tooltip + "," + tData[i].ath + "," + tData[i].atv + ")");
          }
        }

        //渲染当前场景标签热点
        var tagLen = result.TagHotspot.length;
        if(tagLen != 0) {
          var tagData = result.TagHotspot;
          for(var i=0;i<tagLen;i++) {
            krpano.call("loadStaticHotspot(" + tagData[i].id + "," + tagData[i].style + "," + tagData[i].tooltip + "," + tagData[i].ath + "," + tagData[i].atv + ")");
          }
        }
      } else {
        krpano.call("error_info_show(热点加载失败！)");
        return;
      }
    }
  });
}

//获取图片和环物缩略图列表（undo）
function getPicList(type, keyword) {
  var krpano = document.getElementById("set_pano");
  var reqUrl = "";
  var picData = null;
  if("photo" == type) {
    reqUrl = "/pic/list";
  } else if("ring" == type) {
    reqUrl = "/ring/list";
  } else {
    krpano.call("error_info_show(图片获取异常，请联系管理员！)");
    return;
  }

  httpService({
    url : reqUrl + "?keywords=" + keyword,
    method : 'GET',
    callback : (data) => {
      if (data && data.code == 200) {
        if(null != data.result && data.result.length != 0) {
          picData = data.result;
        } else {
          krpano.call("error_info_show(没有更多的图片了！)");
          return;
        }
      } else {
        krpano.call("error_info_show(图片获取异常，请联系管理员！)");
        return;
      }
    }
  });
  return picData;
}

//获取场景缩略图
function getSceneList(sceneId) {
  var krpano = document.getElementById("set_pano");
  var sceneData = null;
  httpService({
    url : "/resource/scene/list?panoId=" + panoId,
    method : 'GET',
    callback : (data) => {
      if (data && data.code == 200) {
        if(null != data.result && data.result.length != 0) {
          sceneData = data.result;
          sceneData.forEach(s => {
            s.coverUrl = imgUrl + s.coverUrl;
          })
        } else {
          krpano.call("error_info_show(没有更多的场景了！)");
          return;
        }
      } else {
        krpano.call("error_info_show(场景获取异常，请联系管理员！)");
        return;
      }
    }
  });
  return sceneData;
}

//获取文本/标签/链接热点信息（undo）
function getOriginalHotspotInfo(type, id) {
  var krpano = document.getElementById("set_pano");
  var reqUrl = "";
  var hotspotData = null;
  if("link" == type) {
    reqUrl = "/pano/linkhotspot?id=" + id;
  } else if("text" == type) {
    reqUrl = "/pano/texthotspot?id=" + id;
  } else if("tags" == type) {
    reqUrl = "/pano/taghotspot?id=" + id;
  } else {
    krpano.call("error_info_show(信息获取异常，请联系管理员！)");
    return;
  }
  httpService({
    url : reqUrl,
    method : 'GET',
    callback : (data) => {
      if (data && data.code == 200) {
        hotspotData = data.result;
      } else {
        krpano.call("error_info_show(信息获取异常，请联系管理员！)");
        return;
      }
    }
  });
  return hotspotData;

}

//初始化视角数据保存
function saveView(sceneId, fov, hlookat, vlookat) {
  var krpano = document.getElementById("set_pano");
  httpService({
    url : '/resource/scene',
    method : 'PUT',
    data : {id: sceneId, fov: fov, hlookat: hlookat, vlookat: vlookat},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(保存成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
      }
    }
  });
}

//保存特效
function saveSnow(snowValue) {
  getData();
  httpService({
    url : '/resource/pano',
    method : 'PUT',
    data : {id: panoId, specialEffect: snowValue},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(保存成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
      }
    }
  });
}

//修改热点坐标
function moveLocation(hotspotId, ath, atv) {
  var krpano = document.getElementById("set_pano");
  httpService({
    url : '/resource/pano/coordinate',
    method : 'PUT',
    data : {id: hotspotId, ath: ath, atv: atv},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(保存成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
        return;
      }
    }
  });
}

//删除热点
function delHotspot(hotspotId) {
  var krpano = document.getElementById("set_pano");
  httpService({
    url : '/resource/pano/hotspot?id=' + hotspotId,
    method : 'DELETE',
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("del_element(" + hotspotId + ")");
        krpano.call("alert_info_show(删除成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
        return;
      }
    }
  });
}

//新增场景切换热点
function addHotspot(hotspotId, sceneId, goScene, style, tooltip) {
  getData();
  httpService({
    url : '/resource/pano/hotspot',
    method : 'POST',
    data: {id: hotspotId, panoId: panoId, sceneId: sceneId, toSceneId: goScene, style: style, tooltip: tooltip},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(保存成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
        return;
      }
    }
  });
}

//修改场景切换热点
function updateHotspot(hotspotId, goScene, tooltip) {
  var krpano = document.getElementById("set_pano");
  httpService({
    url : '/resource/pano/hotspot',
    method : 'PUT',
    data: {id: hotspotId, toSceneId: goScene, tooltip: tooltip},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(修改成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
        return;
      }
    }
  });
}

//新增环物热点（undo）
function addRingHotspot(hotspotId, sceneId, style, tooltip, ringId) {
  getData();
  httpService({
    url : '/pano/ringhotspot',
    method : 'POST',
    data: {id: hotspotId, panoId: panoId, sceneId: sceneId, style: style, tooltip: tooltip, ringId: ringId},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(修改成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
        return;
      }
    }
  });
}

//新增图片热点（undo）
function addPicHotspot(hotspotId, sceneId, style, tooltip, picIds) {
  getData();
  httpService({
    url : '/pano/pichotspot',
    method : 'POST',
    data: {id: hotspotId, panoId: panoId, sceneId: sceneId, style: style, tooltip: tooltip, picIds: picIds},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(保存成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
        return;
      }
    }
  });
}

//新增链接热点（undo）
function addLinkHotspot(hotspotId, sceneId, style, tooltip, link) {
  getData();
  httpService({
    url : '/pano/linkhotspot',
    method : 'POST',
    data: {id: hotspotId, panoId: panoId, sceneId: sceneId, style: style, tooltip: tooltip, link: link},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(保存成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
        return;
      }
    }
  });
}

//修改链接热点信息（undo）
function updateLinkHotspot(hotspotId, tooltip, link) {
  var krpano = document.getElementById("set_pano");
  httpService({
    url : '/pano/linkhotspot',
    method : 'PUT',
    data: {id: hotspotId, tooltip: tooltip, link: link},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(保存成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
        return;
      }
    }
  });
}

//新增文本热点（undo）
function addTextHotspot(hotspotId, sceneId, style, tooltip, content) {
  getData();
  httpService({
    url : '/pano/texthotspot',
    method : 'POST',
    data: {id: hotspotId, panoId: panoId, sceneId: sceneId, style: style, tooltip: tooltip, content: content},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(保存成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
        return;
      }
    }
  });

}

//修改文本热点信息（undo）
function updateTextHotspot(hotspotId, tooltip, content) {
  var krpano = document.getElementById("set_pano");
  httpService({
    url : '/pano/texthotspot',
    method : 'PUT',
    data: {id: hotspotId, tooltip: tooltip, content: content},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(保存成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
        return;
      }
    }
  });

}

//新增标签热点（undo）
function addTagHotspot(hotspotId, sceneId, style, tooltip, tag) {
  getData();
  httpService({
    url : '/pano/taghotspot',
    method : 'POST',
    data: {id: hotspotId, panoId: panoId, sceneId: sceneId, style: style, tooltip: tooltip, tag: tag},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(保存成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
        return;
      }
    }
  });

}

//修改标签热点信息（undo）
function updateTagHotspot(hotspotId, tooltip, tag) {
  var krpano = document.getElementById("set_pano");
  httpService({
    url : '/pano/taghotspot',
    method : 'PUT',
    data: {id: hotspotId, tooltip: tooltip, tag: tag},
    callback : (data) => {
      if (data && data.code == 200) {
        krpano.call("alert_info_show(保存成功！)");
      } else {
        krpano.call("error_info_show(请求异常，请联系管理员！)");
        return;
      }
    }
  });
}


