import Vue from 'vue'
Vue.filter('isRecommendFilter',function(value){ //推荐到首页状态
  switch (value){
    case 1:
      return '未推荐';
      break;
    case 2:
      return '推荐审核中';
      break;
    case 3:
      return '通过';
      break;
    case 4:
      return '不通过';
      break;
    default:
      break;
  }
})
Vue.filter('isCheckFilter',function(value){ //推荐到首页状态
  switch (value){
    case 1:
      return '审核中';
      break;
    case 2:
      return '待二次审核';
      break;
    case 3:
      return '审核通过';
      break;
    case 4:
      return '审核不通过';
      break;
    default:
      break;
  }
})
Vue.filter('userType',function(value){
  switch (value){
    case '1':
      return 'VR个人工作室';
      break;
    case '2':
      return 'VR企业';
      break;
    case '3':
      return 'VR开发区';
      break;
    case '4':
      return 'VR招商局';
      break;
    default:
      break;
  }
})
Vue.filter('dateFormat',function(value,str){ //格式化时间
  if (str) {
    return moment(value).format(str)
  }
  return moment(value).format('YYYY-MM-DD');
})
Vue.filter('strToArray',function(value,val){ //string转数组
  return value.split(val)
})
Vue.filter('dateSimply',function(value){ //日期 年月日
  let spaceNum=value.indexOf(' ');
  return value.substring(0,spaceNum)
})
Vue.filter('publishStatus',function(value){ //发布的状态转换
  switch (value){
    case -1:
          return '未发布';
    case 1:
          return '已发布'
  }
})
Vue.filter('publishStatusBtn',function(value){ //发布的状态转换
  switch (value){
    case -1:
      return '发布';
    case 1:
      return '取消发布'
  }
})


