export default{
  assignObj(target, source) {
    let keyArr = [];
    for (var k in target) {
      keyArr.push(k);
    }
    for (var obj in source) {
      let bHave = null;
      bHave = keyArr.filter((child) => {
        return child == obj;
      });
      if (bHave && bHave.length > 0) {
        target[obj] = source[obj];
      }
    }
    return target;
  },
  install(Vue,options){
    Vue.prototype.newWin=function(url){ //新窗口打开
      var a = document.createElement('a');
      a.setAttribute('href', url);
      a.setAttribute('style', 'display:none');
      a.setAttribute('target', '_blank');
      document.body.appendChild(a);
      a.click();
      a.parentNode.removeChild(a);
    };
    Vue.prototype.isPC=function(){  //判断是否为pc端
      var sUserAgent = navigator.userAgent.toLowerCase();
      var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
      var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
      var bIsMidp = sUserAgent.match(/midp/i) == "midp";
      var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
      var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
      var bIsAndroid = sUserAgent.match(/android/i) == "android";
      var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
      var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
      if (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM) {
        return false;
      } else {
        return true;
      }
    };
    Vue.prototype.setTitle=function(title){ //设置title解决iOS的微信上是不兼容的
      var u = navigator.userAgent;
      var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1
      var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)
      if (isAndroid) {
        document.title = title
      } else if (isiOS) {
        var $body = $('body')
        document.title = title
        var $iframe = $('<iframe src="/favicon.ico"></iframe>')
        $iframe.on('load', function () {
          window.setTimeout(function () {
            $iframe.off('load').remove()
          }, 0)
        }).appendTo($body)
      }
    };
    Vue.prototype.setCookie=function(cname,cvalue,exdays){
      var d=new Date();
      d.setTime(d.getTime()+(exdays*24*60*60*1000));
      var expires="expires="+d.toGMTString();
      document.cookie=cname+'='+cvalue+"; "+expires;
    };
    Vue.prototype.getCookie=function(){
      var name=cname+'=';
      var ca=document.cookie.split(";");
      for(var i=0;i<ca.length;i++){
        var c=ca[i].trim();
        if(c.indexOf(name)==0){
          return c.substring(name.length,c.length)
        }
      }
      return "";
    };
    Vue.prototype.easyClone=function(origin){
        if(typeof origin!=='object') return origin;
        var result=Object.prototype.toString.call(origin)=='[object Object]'?{}:[];
        for(var key in origin){
          if(origin.hasOwnProperty(key)){
            result[key]=origin[key]
          }
        }
        return result;
    };
    Vue.prototype.deepClone=function(origin){
      if(typeof origin!=='object') return origin;
      var result=Object.prototype.toString.call(origin)=='[object Object]'?{}:[];
      for(var key in origin){
        if(origin.hasOwnProperty(key)){
          result[key]=(origin[key]!=='null'&&typeof origin[key])=='object'?Vue.prototype.deepClone(origin[key]):origin[key]
        }
      }
      return result;
    };
    Vue.prototype.browser= {
      versions:function(){
        let u = navigator.userAgent, app = navigator.appVersion;
        return {
          trident: u.indexOf('Trident') > -1, //IE内核
          presto: u.indexOf('Presto') > -1, //opera内核
          webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
          gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1,//火狐内核
          mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
          ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
          android: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, //android终端
          iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
          iPad: u.indexOf('iPad') > -1, //是否iPad
          webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
          weixin: u.indexOf('MicroMessenger') > -1, //是否微信 （2015-01-22新增）
          qq: u.match(/\sQQ/i) == " qq" //是否QQ
        }
      }(),
      language:(navigator.browserLanguage || navigator.language).toLowerCase()
   };
    Vue.prototype.uploadImg=(function(){
      function getObjectUrl(file){ //getObjectURL方法用于获取本地图片地址,使用该URL可以显示图片
        var url;
        if(window.createObjectUrl!=undefined){ //basic
          url=window.createObjectURL(file);
        }else if(window.URL!=undefined){ //mozilla(firefox)
          url=window.URL.createObjectURL(file);
        }else if(window.webkitURL!=undefined){ //webkit or chrome
          url=window.webkitURL.createObjectURL(file);
        }
        return url;
      }
      return {
        getObjectUrl:getObjectUrl
      }
    })();
    /**
     * 检查是否为网址
     * @param {}
     * str_url
     * @return {Boolean}true：是网址，false:<b>不是</b>网址;
     */
    Vue.prototype.isURL=function(str_url){ // 验证url
      var reg=/(^#)|(^http(s*):\/\/[^\s]+\.[^\s]+)/
      return reg.test(str_url);
    };
    Vue.prototype.offsetLT=function(curEle){
      var l=curEle.offsetLeft;
      var t=curEle.offsetTop;
      var p=curEle.offsetParent;
      while(p.tagName!=='BODY'){
        l+= p.clientLeft;
        t+= p.clientTop;
        if(!/MSIE 8/i.test(navigator.userAgent)){
          l+= p.offsetLeft;
          t+= p.offsetTop;
        }
        p= p.offsetParent;
      }
      return {top:t,left:l}
    };
    Vue.prototype.highlightWord=function(originalData,keyword,color='#bd3232'){
      const reg=new RegExp(keyword,'i');
      let dom=document.createElement('div');
      $(dom).html(originalData)
      let conStr=$(dom).text();
      if(originalData){
        if(keyword&&keyword.toString()){
          return conStr.replace(reg,`<span style="color:${color};font-weight:700">${keyword}</span>`)
        }else{
          return conStr;
        }}
       dom=null;
      };
    Vue.prototype.hideOverHighlight=function(originalData,keyword,color='#bd3232'){

        const reg=new RegExp(keyword,'i');
        let dom=document.createElement('div');
        $(dom).html(originalData)
        let conStr=''+$(dom).text();
        let indexRes=0;
        let ellipsisFlag='';
        if(originalData){
          if(keyword!==undefined&&keyword.toString().length>0){
            let keywordIndex=conStr.indexOf(keyword);
            indexRes=keywordIndex-10;
            if(indexRes<0){
              indexRes=0;
            }else{
              ellipsisFlag='···'
            }
            conStr=conStr.substr(indexRes,200);
            return  ellipsisFlag+conStr.replace(reg,`<span style="color:${color};font-weight:700">${keyword}</span>`)
          }else{
            return conStr;
          }
        }
         dom=null;

      },
    Vue.prototype.doWxConfig=function(data){
      let desc = '智招网——首家政府、企业、人才、智慧招商大数据VR云平台';
      this.fetchWXParams().then(res => {
        if (res) {
          // console.log('我进来在这里哟');
          // console.log(data);
          wx.config({
            debug: false,
            appId: res.appid,
            timestamp: res.timestamp,
            nonceStr: res.nonceStr,
            signature:res.signature,
            jsApiList: [
              'onMenuShareTimeline',
              'onMenuShareAppMessage',
              'onMenuShareQQ',
              'onMenuShareWeibo',
              'onMenuShareQZone'
            ]
          });
          wx.ready(function(){
            wx.checkJsApi({
              jsApiList: [
                'onMenuShareTimeline',
                'onMenuShareAppMessage',
                'onMenuShareQQ',
                'onMenuShareWeibo',
                'onMenuShareQZone'
              ]
            });
            /*分享到朋友圈*/
            wx.onMenuShareTimeline({
              title: data ? data.title : '智招网1',
              desc: data.newsAbstract ? data.newsAbstract : desc,
              link: location.href,
              imgUrl: 'http://cdn.zhizhaowang.cn/dWw72vBTwT4o3dY.jpg',
              success: function () {

              },
              cancel: function () {

              }
            });

            /*分享给朋友*/
            wx.onMenuShareAppMessage({
              title: data ? data.title : '智招网1',
              desc: data.newsAbstract ? data.newsAbstract : desc,
              link: location.href,
              imgUrl: 'http://cdn.zhizhaowang.cn/dWw72vBTwT4o3dY.jpg',
              type: 'link', // 分享类型,music、video或link，不填默认为link
              dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
              success: function () {

              },
              cancel: function () {

              }
            });

            wx.onMenuShareQQ({
              title: data ? data.title : '智招网1',
              desc: data.newsAbstract ? data.newsAbstract : desc,
              link: location.href,
              imgUrl: 'http://cdn.zhizhaowang.cn/dWw72vBTwT4o3dY.jpg',
              success: function () {

              },
              cancel: function () {

              }
            });

            wx.onMenuShareWeibo({
              title: data ? data.title : '智招网1',
              desc: data.newsAbstract ? data.newsAbstract : desc,
              link: location.href,
              imgUrl: 'http://cdn.zhizhaowang.cn/dWw72vBTwT4o3dY.jpg',
              success: function () {

              },
              cancel: function () {

              }
            });

            wx.onMenuShareQZone({
              title: data ? data.title : '智招网1',
              desc: data.newsAbstract ? data.newsAbstract : desc,
              link: location.href,
              imgUrl: 'http://cdn.zhizhaowang.cn/dWw72vBTwT4o3dY.jpg',
              success: function () {

              },
              cancel: function () {

              }
            });
          });
        }
      });
    },
    Vue.prototype.fetchWXParams=function(){
      let self = this;
      return new Promise(function (resolve,reject) {
        axios.get('http://www.zhizhaowang.cn:8088/index/token?url='+encodeURIComponent(location.href.split('#')[0])).then((_data)=>{
          if(_data.data.code===200){
            resolve(_data.data.result);
          }else{
            reject();
          }
        },()=>{
          reject(null);
        })
      });
    }
  }
}


