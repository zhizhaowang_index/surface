// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import App from './App'
import {routes} from './router/router'
import axios from 'plugins/axios'
import {_store} from 'store/index'
import httpClient from 'plugins/httpClient'

import VueMeta from 'vue-meta'
import myPlugin from './util/util'
import util from './util/util'

import 'animate.css'
import 'babel-polyfill'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'
import 'src/assets/css/icont/iconfont.css'
import 'src/assets/css/reset.css'
import 'src/filter/index.js'


import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
Vue.config.devtools = true;
//全局配置bus对象
window.eventBus = new Vue();
Vue.config.productionTip = false;


Vue.use(axios)
Vue.use(httpClient)
Vue.use(VueMeta)

Vue.use(myPlugin)
Vue.use(util)
Vue.use(VueAwesomeSwiper)



const store=new Vuex.Store(_store)
Vue.config.productionTip = false
const router=new VueRouter({
  mode:'history',
  routes:routes,
  node:'http',
  scrollBehavior(to,from,savedPosition){
    if(savedPosition){
      return savedPosition
    }else{
      if(from.meta.keepAlive){
        from.meta.savedPosition=document.body.scrollTop
      }
      return {x:0,y:to.meta.savedPosition||0}
    }
    if (to.hash) {
      return {selector: to.hash} //锚点定位
    }
  }
})
router.beforeEach((to, from, next) => {
  if(to.matched.some(res=>res.meta.requiredAuth)){ //判断是否需要登录权限 to.meta.requiredAuth or to.matched.some(res=>res.meta.requiredAuth)
    if(localStorage.getItem('currentUser')){
        next();
    }else{
      next({
        path:'/index/login',
        query:{redirect: to.fullPath} // 将跳转的路由path作为参数，登录成功后跳转到该路由
      })
    }
  }else{
    next();
  }

});
router.afterEach(route => {
  // LoadingBar.finish();
});
/* eslint-disable no-new */
Vue.directive('focus', {
  // 当绑定的元素插入到 DOM 时调用此函数……
  inserted: function (el) {
    // 元素调用 focus 获取焦点
    el.focus()
  }
})
new Vue({
  el: '#app',
  router:router,
  store:store,
  template: '<App/>',
  components: { App }
})
