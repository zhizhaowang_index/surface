import {SERVICE_URLS} from 'config/service.url.js'
export const _store = {
  state: {
    isLogin:false,
    imgPrefix : '',//头部公用请求地址
    currentUser:null, //当前用户
    columnSel:[], //vr管理勾选栏目
    avatar:null,
    panoId:null, //存储最新的panoId
    globalSearchType:1,  //全局搜索globalSearchType
    areaCode:''
  },
  getters : {
    imgPrefix : state => {
      let url = state.imgPrefix;
      if (url) {
        return url;
      }
      return '';
    },
    currentUser : state=> {
      let user = state.currentUser;
      if (!user) {
        let str = localStorage.getItem('currentUser') || null;
        if (!str) {
          user = null;
        }
        user = JSON.parse(str);
      }
      return user;
    },
    currentPanoId:state=>{
      let panoId = state.panoId;
      if(!panoId){
        let ls_PanoId=localStorage.getItem('panoId');
        if(ls_PanoId){
          return ls_PanoId;
        }
      }
      return panoId;
    }
  },
  mutations: {
    setPanoId(state,panoId){
      localStorage.setItem('panoId',panoId)
      state.panoId=panoId;
    },
    setCurrentUser(state,user) {
      state.currentUser = user;
    },
    setImgPrefix (state,imgPrefix) {
      state.imgPrefix = imgPrefix;
    },
    changeAvatar:function(state,data){
      state.avatar=data;
    },
    setProjectUrl(state,projectUrl){
      state.projectUrl=projectUrl;
    },
    setAreaCode(state,areaCode){
      state.areaCode=areaCode;
    },
  },
  actions: {
    setImgPrefix (context) {
      if(context.state.imgPrefix){
        return;
      }
      var xhr=new XMLHttpRequest();
      xhr.open('GET',SERVICE_URLS.commonUrl.imgUrl.path,false)
      xhr.onreadystatechange=function(){
        if(xhr.readyState===4&&xhr.status===200){
          var result=JSON.parse(xhr.responseText);
          var _result=result.result.ImgHeadUrl;
          context.commit('setImgPrefix',_result);
        }
      }
      xhr.send(null);
    }
  }
}

