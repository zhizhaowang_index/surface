import Login from '../components/login/login/index.vue'
import findPwd from '../components/login/findPassWord/index.vue'
import Index from '../components/index.vue'
import Accident from '../components/accident/index.vue'

const Register = resolve => require(['components/login/register/index.vue'], resolve)
const homeRegister = resolve => require(['components/login/homeRegister/index.vue'], resolve)
const successRegister = resolve => require(['components/login/successRegister/index.vue'], resolve)
const userAgreetment = resolve => require(['components/login/userAgreetment/index.vue'], resolve)
const userType = resolve => require(['components/login/userType/index.vue'], resolve)
const Home = resolve => require(['components/pages/home/index.vue'], resolve)
const Video = resolve => require(['components/pages/video/index.vue'], resolve)
const serviceInfoList = resolve => require(['components/pages/home/serviceInfoList/index.vue'], resolve)
const serviceInfoDetail = resolve => require(['components/pages/home/serviceDetail/index.vue'], resolve)
const vrList = resolve => require(['components/pages/vrList/index.vue'], resolve)
const tradeInfo = resolve => require(['components/pages/vrInfo/index.vue'], resolve)
const channel = resolve => require(['components/accident/channel/index.vue'], resolve)
const channelList = resolve => require(['components/accident/channel/list.vue'], resolve)
const infolist = resolve => require(['components/accident/channel/infolist.vue'], resolve)
const situation = resolve => require(['components/accident/channel/situation.vue'], resolve)
const newsDetails = resolve => require(['components/accident/channel/newsDetails.vue'], resolve)

const infoDetail = resolve => require(['components/pages/vrInfo/infoDetail.vue'], resolve)
const userMaterial = resolve => require(['components/pages/userMaterial/index.vue'], resolve)
const vrShow = resolve => require(['components/pages/home/vrShow/index.vue'], resolve)
const appVRShow = resolve => require(['components/pages/home/vrShow/app.vue'], resolve)
const cityInfo = resolve => require(['components/pages/cityInfo/index.vue'], resolve)
// const serviceInfoList = resolve => require(['components/pages/home/serviceInfoList/index.vue'], resolve)


const personal = resolve => require(['components/pages/personalCenter/index.vue'], resolve)
const map = resolve => require(['components/pages/personalCenter/map/index.vue'], resolve)
const companyInfo = resolve => require(['components/pages/personalCenter/map/companyInfo.vue'], resolve)
const newsPush = resolve => require(['components/pages/personalCenter/news/push/index.vue'], resolve)
const newsRelease = resolve => require(['components/pages/personalCenter/news/release/index.vue'], resolve)
const vrPreview = resolve => require(['components/pages/personalCenter/VRmanage/preview/index.vue'], resolve)


const myVr = resolve => require(['components/pages/personalCenter/VRManage/my-vr/index.vue'], resolve)
const music = resolve => require(['components/pages/personalCenter/VRManage/music/index.vue'], resolve)
const picture = resolve => require(['components/pages/personalCenter/VRManage/picture/index.vue'], resolve)
const annular = resolve => require(['components/pages/personalCenter/VRManage/annular/index.vue'], resolve)
const video = resolve => require(['components/pages/personalCenter/VRManage/video/index.vue'], resolve)
const vrSet = resolve => require(['components/pages/personalCenter/VRManage/my-vr/vr-set.vue'], resolve)
const vrView = resolve => require(['components/pages/personalCenter/VRManage/my-vr/vr-view.vue'], resolve)
const vrStore = resolve => require(['components/pages/personalCenter/VRManage/vrStore/index.vue'], resolve)
//发布服务
const PublishService = resolve => require(['components/pages/personalCenter/publishService/index.vue'], resolve)
const InvestService = resolve => require(['components/pages/personalCenter/publishService/investService/index1.vue'], resolve)
const FinanceService = resolve => require(['components/pages/personalCenter/publishService/financeService/index1.vue'], resolve)
const AttractService = resolve => require(['components/pages/personalCenter/publishService/attractService/index1.vue'], resolve)
const CommercialService = resolve => require(['components/pages/personalCenter/publishService/commercialService/index1.vue'], resolve)
const DeclareMoney = resolve => require(['components/pages/personalCenter/publishService/declareMoney/index.vue'], resolve)
const ExpertConsult = resolve => require(['components/pages/personalCenter/publishService/expertConsult/index1.vue'], resolve)
const JobApplication = resolve => require(['components/pages/personalCenter/publishService/jobApplication/index.vue'], resolve)
const OfferMeet = resolve => require(['components/pages/personalCenter/publishService/offerMeet/index1.vue'], resolve)
const ProjectSelect = resolve => require(['components/pages/personalCenter/publishService/projectSelect/index1.vue'], resolve)
const TechnologyService = resolve => require(['components/pages/personalCenter/publishService/technologyService/index1.vue'], resolve)
const TrainAdmission = resolve => require(['components/pages/personalCenter/publishService/trainAdmission/index1.vue'], resolve)
//服务列表
const ServiceList = resolve => require(['components/pages/personalCenter/serviceList/index2.vue'], resolve)
const previewServiceDetail = resolve => require(['components/pages/personalCenter/previewDetail/index.vue'], resolve)
//服务收藏
const storeService = resolve => require(['components/pages/personalCenter/storeService/index.vue'], resolve)
const vrOperate = resolve => require(['components/pages/personalCenter/operate/index.vue'], resolve)
const urlShare = resolve => require(['components/pages/personalCenter/VRManage/urlShare/index.vue'], resolve)
const securitySet = resolve => require(['components/pages/personalCenter/accountSet/securitySet/index1.vue'], resolve)


const userDataSet = resolve => require(['components/pages/personalCenter/accountSet/userData/index1.vue'], resolve)
const userDataVerify = resolve => require(['components/pages/personalCenter/accountSet/verifyIdentity/index.vue'], resolve)
// 省级频道
const swipermanagement = resolve => require(['components/pages/personalCenter/province/carousel/index.vue'], resolve)
const localSituation = resolve => require(['components/pages/personalCenter/province/localSituation/index.vue'], resolve)
const policy = resolve => require(['components/pages/personalCenter/province/policy/index.vue'], resolve)
const policyAdd = resolve => require(['components/pages/personalCenter/province/policy/add.vue'], resolve)
const merchants = resolve => require(['components/pages/personalCenter/province/merchants/index.vue'], resolve)
const merchantsAdd = resolve => require(['components/pages/personalCenter/province/merchants/add.vue'], resolve)
const library = resolve => require(['components/pages/personalCenter/province/library/index.vue'], resolve)
const intention = resolve => require(['components/pages/personalCenter/province/intention/index.vue'], resolve)

const allrelease = resolve => require(['components/pages/personalCenter/province/release/index.vue'], resolve)
const allreleaseAdd = resolve => require(['components/pages/personalCenter/province/release/add.vue'], resolve)


const newsDetail = resolve => require(['components/pages/home/news-detail/index.vue'], resolve)
const newsColumn = resolve => require(['components/pages/home/newsList/index.vue'], resolve)
const hotNewsColumn = resolve => require(['components/pages/home/newsList/hotNewsList.vue'], resolve)

const columnMenu = () => import('components/pages/personalCenter/VRmanage/columnMenu/index.vue')
const columnContent = () => import('components/pages/personalCenter/VRmanage/columnContent/index.vue')
const columnList = () => import('components/pages/personalCenter/VRmanage/columnList/index.vue')
const vrShowConList = () => import('components/pages/personalCenter/VRmanage/preview/contentList.vue')
const vrShowConDetail = () => import('components/pages/personalCenter/VRmanage/preview/contentDetail.vue')
const globalSearch = () => import('components/pages/home/globalSearch/index.vue')
const webPage = () => import('components/pages/webPage/index.vue')

export const routes = [{
    path: '',
    component: Index,
    children: [{
      path: '',
      component: Home
    }]
  },
  {
    path: '/accident',
    component: Accident,
    children: [{
        path: 'channel',
        component: channel
      },
      {
        path: 'channelList',
        component: channelList
      },
      {
        path: 'infolist',
        component: infolist
      },
      {
        path: 'situation',
        component: situation
      },
      {
        path: 'newsDetails',
        component: newsDetails
      },
      
    ]
  },
  {
    path: '/index',
    component: Index,
    children: [{
        path: '',
        redirect: 'home'
      },
      {
        path: 'search',
        component: globalSearch
      },
      {
        path: 'vrList',
        component: vrList
      },
      {
        path: 'userMaterial',
        component: userMaterial
      },
      {
        path: 'vrInfo',
        component: tradeInfo
      },
      {
        path: 'infoDetail/:unitId/:type',
        component: infoDetail
      },
      {
        path: 'home',
        component: Home,
      },
      {
        path: 'video',
        component: Video,
      },
      {
        path: 'serviceInfoList',
        component: serviceInfoList
      },
      {
        path: 'serviceInfoDetail/:type/:id/:creator',
        component: serviceInfoDetail
      },
      {
        path: 'newsColumn/:pId/:title',
        component: newsColumn,
        name: 'newsColumn'
      },
      {
        path: 'previewServiceDetail/:id/:type',
        component: previewServiceDetail,
        name: 'previewServiceDetail'
      },
      {
        path: 'newsDetail',
        component: newsDetail,
        name: 'newsDetail'
      },
      {
        path: 'hotNewsList',
        component: hotNewsColumn,
        name: 'hotNesList'
      },
      {
        path: 'login',
        component: Login,
        beforeEnter: (to, from, next) => {
          localStorage.clear();
          next(true);
        }
      },
      {
        path: 'findPwd',
        component: findPwd
      },
      {
        path: 'register',
        component: Register
      },
      {
        path: 'userType',
        component: userType
      },
      {
        path: 'userAgreetment',
        component: userAgreetment
      },
      {
        path: 'homeRegister',
        component: homeRegister
      },
      {
        path: 'successRegister',
        component: successRegister
      },
      {
        path: 'cityList/:pId/:zone',
        name: 'cityList',
        component: cityInfo
      },
      {
        path: 'personal',
        component: personal,
        meta: {
          requiredAuth: true
        },
        children: [{
            path: '',
            redirect: '/index/personal/userDataSet'
          }, {
            path: 'newsPush',
            meta: {
              requiredAuth: true
            },
            component: newsPush
          }, {
            path: 'newsRelease',
            meta: {
              requiredAuth: true
            },
            component: newsRelease
          },
          {
            path: 'vrMenu', //全景菜单
            meta: {
              requiredAuth: true
            },
            component: columnMenu
          },
          {
            path: 'vrContent',
            meta: {
              requiredAuth: true
            },
            component: columnList
          },
          {
            path: 'vrContent/:type/:id',
            name: 'vrContentList',
            meta: {
              requiredAuth: true
            },
            component: columnContent
          },
          {
            path: 'vrRelease',
            meta: {
              requiredAuth: true
            },
            component: columnContent
          },
          {
            path: 'vrPreview',
            meta: {
              requiredAuth: true
            },
            component: vrPreview,
          },
          {
            path: 'my-vr',
            meta: {
              requiredAuth: true
            },
            component: myVr
          },
          {
            path: 'music',
            meta: {
              requiredAuth: true
            },
            component: music
          },
          {
            path: 'picture',
            meta: {
              requiredAuth: true
            },
            component: picture
          },
          {
            path: 'annular',
            meta: {
              requiredAuth: true
            },
            component: annular
          },
          {
            path: 'video',
            meta: {
              requiredAuth: true
            },
            component: video
          },
          {
            path: 'vrStore',
            meta: {
              requiredAuth: true
            },
            component: vrStore
          },
          {
            path: 'publishService',
            meta: {
              requiredAuth: true
            },
            component: PublishService,
            children: [{
                path: '',
                redirect: '/index/personal/publishService/investService'
              },
              {
                path: 'investService',
                meta: {
                  requiredAuth: true
                },
                component: InvestService
              },
              {
                path: 'financeService',
                meta: {
                  requiredAuth: true
                },
                component: FinanceService
              },
              {
                path: 'attractService',
                meta: {
                  requiredAuth: true
                },
                component: AttractService
              },
              {
                path: 'commercialService',
                meta: {
                  requiredAuth: true
                },
                component: CommercialService
              },
              {
                path: 'declareMoney',
                meta: {
                  requiredAuth: true
                },
                component: DeclareMoney
              },
              {
                path: 'expertConsult',
                meta: {
                  requiredAuth: true
                },
                component: ExpertConsult,
              },
              {
                path: 'jobApplication',
                meta: {
                  requiredAuth: true
                },
                component: JobApplication,
              },
              {
                path: 'offerMeet',
                meta: {
                  requiredAuth: true
                },
                component: OfferMeet
              },
              {
                path: 'projectSelect',
                meta: {
                  requiredAuth: true
                },
                component: ProjectSelect
              },
              {
                path: 'technologyService',
                meta: {
                  requiredAuth: true
                },
                component: TechnologyService,
              },
              {
                path: 'trainAdmission',
                meta: {
                  requiredAuth: true
                },
                component: TrainAdmission
              }

            ]
          },
          {
            path: 'serviceList',
            meta: {
              requiredAuth: true
            },
            component: ServiceList
          },
          {
            path: 'storeService',
            meta: {
              requiredAuth: true
            },
            component: storeService
          },

          {
            path: 'vrOperate',
            meta: {
              requiredAuth: true
            },
            component: vrOperate
          },
          {
            path: 'urlShare',
            meta: {
              requiredAuth: true
            },
            component: urlShare
          },
          {
            path: 'securitySet', //安全设置
            meta: {
              requiredAuth: true
            },
            component: securitySet
          },
          {
            path: 'userDataSet', //用户资料修改设置
            meta: {
              requiredAuth: true
            },
            component: userDataSet
          },
          {
            path: 'userDataVerify', //认证
            meta: {
              requiredAuth: true
            },
            component: userDataVerify
          },
          /////////
          {
            path: 'swiper',
            meta: {
              requiredAuth: true
            },
            component: swipermanagement
          },
          {
            path: 'localSituation',
            meta: {
              requiredAuth: true
            },
            component: localSituation
          },
          {
            path: 'policy',
            meta: {
              requiredAuth: true
            },
            component: policy
          },
          {
            path: 'policyAdd',
            name: 'policyAdd',
            meta: {
              requiredAuth: true
            },
            component: policyAdd
          },

          {
            path: 'merchants',
            meta: {
              requiredAuth: true
            },
            component: merchants
          },
          {
            path: 'merchantsAdd',
            name: 'merchantsAdd',
            meta: {
              requiredAuth: true
            },
            component: merchantsAdd
          },
          {
            path: 'allrelease',
            name: 'allrelease',
            meta: {
              requiredAuth: true
            },
            component: allrelease
          },
          {
            path: 'allreleaseAdd',
            name: 'allreleaseAdd',
            meta: {
              requiredAuth: true
            },
            component: allreleaseAdd
          },

          {
            path: 'library',
            meta: {
              requiredAuth: true
            },
            component: library
          },
          {
            path: 'intention',
            meta: {
              requiredAuth: true
            },
            component: intention
          },
        ]
      }
    ]
  },
  {
    path: "/vrPreview",
    component: vrPreview,
  },
  {
    path: "/web-page",
    component: webPage,
  },
  {
    path: "/vrPreview/list/:columnId/:columnName",
    component: vrShowConList,
    name: 'vrShowConList'
  },
  {
    path: "/vrPreview/detailInfo/:type/:id",
    component: vrShowConDetail,
    name: 'vrShowConDetail'
  },
  {
    path: '/map',
    meta: {
      requiredAuth: true
    },
    component: map
  },
  {
    path: '/companyInfo/:companyId',
    component: companyInfo
  },
  {
    path: "/vrShow",
    name: 'vrShow',
    component: vrShow
  },
  {
    path: '/vrShow_app', //用于app端使用
    component: appVRShow
  },
  {
    path: "/vrView",
    component: vrView
  },
  {
    path: "/vr-set",
    component: vrSet
  }
]
