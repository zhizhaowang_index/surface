export const pictureSearchVo = function () {
  return new Object({
    keyword : '',
    panoId : '',
    pageNo: '',
    pageSize: '',
    total: 0,
    currentPage: 1
  });
};
