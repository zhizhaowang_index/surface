export const vrSearchVo = function () {
    return new Object({
        keyword : '',
        areaId : '',
        pageNo: '',
        pageSize: '',
        total: 0,
        currentPage: 1
    });
};
