export const musicSearchVo = function () {
  return new Object({
    keyword : '',
    pageNo: '',
    pageSize: '',
    total: 0,
    currentPage: 1
  });
};
