export const vrUpdateVo = function () {
    return new Object({
      id: '',
      panoName: '',
      panoId : '',
      tagsId: '',
      description: '',
      coverUrl: '',
      musicId: '',
      isPlay: '',
      pgro: '',
      place: '',
      star: '',
      parade: '',
      outlink: '',
      isPatchTop: '',
      isPatchBot: '',
      patchArr : [],
      patchUrl: '',
      specialEffect: '',
      parameterArr : [],
      playMode : 'scene'
    });
};
