export const musicSearchVo = function () {
  return new Object({
    keyword : '',
    panoId : '',
    pageNo: '',
    pageSize: '',
    total: 0,
    currentPage: 1
  });
};
