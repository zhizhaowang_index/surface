import {ENV} from '../config/environment'
import Vuex from 'vuex'
let baseUrl = "http://127.0.0.1:9900";
const devUrl = "http://test.zhizhaowang.cn:8882";
// const devUrl = "http://192.168.0.109:8882";
const productUrl = "http://www.zhizhaowang.cn:8088";

if (ENV == "dev") {
  baseUrl = devUrl;
} else if (ENV == "product") {
  baseUrl = productUrl;
}

export const PROJECT_URL=baseUrl;
export  const SERVICE_URLS = {
  test:{//测试接口
    addColumn:{ //新增全景菜单
      path:baseUrl+'/front/panomenu',
      method:'POST'
    },
    editColumn:{
      path:baseUrl+'/front/panomenu',
      method:'put'
    }
  },
  //全局搜索
  globalSearch:{
    path:baseUrl+'/index/global/search?pageNo={pageNo}&pageSize={pageSize}&keyword={keyword}&sort={sort}',
    method:'GET',
  },
  globalSearch_type4Char:{ //4中类型政企信息
    path:baseUrl+'/index/global/search?pageNo={pageNo}&pageSize={pageSize}&keyword={keyword}&sort={sort}&type={type}',
    method:'GET'
  },
  globalSearch_typeService:{ //4中类型政企信息
    path:baseUrl+'/index/global/search?pageNo={pageNo}&pageSize={pageSize}&keyword={keyword}&sort={sort}&serviceType={serviceType}',
    method:'GET'
  },
  //上下游接口
  UD:{
    productList:{ //根据输入的产品关键字查询相关产品列表
      path:baseUrl+'/commercialreaches/product/word?keyword={keyword}',
      method:'GET'
    },
    provinceCount:{ //获取省市
      path:baseUrl+'/commercialreaches/company/lev?level={level}&countryCode={countryCode}',
      method:'GET'
    },
    cityCount:{ //查询城市
      path:baseUrl+'/commercialreaches/company/id?id={id}&countryCode={countryCode}',
      method:'GET'
    },
    productCompany:{
      path:baseUrl+'/commercialreaches/company/list?areaId={areaId}&level={level}&countryCode={countryCode}&pageNo={pageNo}&pageSize={pageSize}',
      method:'GET'
    },
    udProduct:{ //获取上下游产品
      path:baseUrl+'/commercialreaches/product/type?tradeCode={tradeCode}&productType={productType}',
      method:'GET'
    },
    wholeLocation:{//根据产品编码获取全国企业坐标
      path:baseUrl+'/commercialreaches/company/all?countryCode={countryCode}',
      method:'GET'
    },
    companyInfo:{ //根据企业id获取企业详情
      path:baseUrl+'/commercialreaches/company/info?id={id}',
      method:'GET'
    },
    addIndustry:{ //新增行业关联
      path:baseUrl+'/usertradeproductrelation/trade',
      method:'POST'
    },
    getIndustry:{ //获取用户关联的行业列表
      path:baseUrl+'/usertradeproductrelation/tradeandproduct?userId={userId}&infoId={infoId}',
      method:'GET'
    },
    deleteIndustry:{ //删除用户相关联行业
      // path:baseUrl+'/usertradeproductrelation?id={id}',
      path:baseUrl+'/usertradeproductrelation/trade?detailTradeCode={detailTradeCode}',
      method:'DELETE'
    },
    deleteIndustryAndProduction:{ //删除行业及其下面所有产品
      path:baseUrl+'/usertradeproductrelation/production/bytrade?detailTradeCode={detailTradeCode}',
      method:'DELETE'
    },
    productDependOnTrade:{ //根据统计局行业编码获取产品列表(排除已选择的产品)
      path:baseUrl+'/commercialreaches/product/countrytradecode?detailTradeCode={detailTradeCode}&countryCodesStr={countryCodesStr}',
      method:'GET'
    },
    editProduct:{//添加或删除用户行业的产品关联
      path:baseUrl+'/usertradeproductrelation/production',
      method:'POST'
    },
    deleteProduction:{ //删除用户产品
      path:baseUrl+'/usertradeproductrelation/production?countryCodes={countryCodes}',
      method:'DELETE'
    },
    productDependOnpCode:{//根据用户产品编码串获取关联的产品
      path:baseUrl+'/commercialreaches/product/user?countryCodesStr={countryCodesStr}',
      method:'GET'
    },
    tradesRelatedWords:{//根据产品名称关键字模糊查询所属上下游行业
      path:baseUrl+'/commercialreaches/tradelist/pname?keyword={keyword}',
      method:'GET'
    },
    proRelatedTrades:{//根据上下游行业编码和产品关键字查询产品
      path:baseUrl+'/commercialreaches/product/tcodeandword?tradeCodes={tradeCodes}&keyword={keyword}',
      method:'GET'
    },
    proDependKeyword:{//根据输入的产品关键字查询相关产品列表
      path:baseUrl+'/commercialreaches/product/word?keyword={keyword}',
      method:'GET'
    }
  },
  //注册借口
  registerManage:{
    personRegister:{//个人注册
      path:baseUrl+'/register/studio',
      method:'POST'
    },
    phoneVerify:{//验证注册手机号
      path:baseUrl+'/register/checkphone?phone={phone}&id={id}&type={type}',
      method:'GET'
    },
    emailVerify:{
      path:baseUrl+'/register/checkemail?email={email}&id={id}&type={type}',
      method:'GET'
    },
    companyRegister:{//企业注册
      path:baseUrl+'/register/company',
      method:'POST'
    },
    developRegister:{//开发区注册
      path:baseUrl+'/register/devzone',
      method:'POST'
    },
    investRegister:{//招商局注册
      path:baseUrl+'/register/merchants',
      method:'POST'
    },
    upLoadImage:{//图片上传
      path:baseUrl+'/upload/pic',
      method:'POST'
    },
    areaList:{//区域列表
      path:baseUrl+'/front/area/list?parentId={parentId}&level={level}',
      method:'GET'
    },
    isLogin:{//验证注册名是否存在
      path:baseUrl+'/register/checkloginname?loginName={loginName}&id={id}&type={type}',
      method:'GET'
    },
    touristRegister:{//游客身份注册
      path:baseUrl+'/register/tourist',
      method:'POST'
    },
    codePhone:{//验证手机号注册码
      path:baseUrl+'/register/sms/validate?phone={phone}&code={code}',
      method:'GET'
    },
    sendPhone:{//发送注册验证码
      path:baseUrl+'/register/sms?phone={phone}',
      method:'GET'
    },
  },
  //获取漫游留言列表
  vrComment:{
    list:{
      path:baseUrl+'/resource/panocomment/list?sceneId={sceneId}&panoId={panoId}&pageNo={pageNo}&pageSize={pageSize}',
      method:'GET'
    },
    comment:{
      path:baseUrl+'/resource/panocomment',
      method:'POST'
    }


  },
  //VR列表
  vrList:{
    path:baseUrl+'/init/front/page?keyword={keyword}&type={type}&pageNo={pageNo}&pageSize={pageSize}&areaId={areaId}&tradeCode={tradeCode}',
    method:'GET'
  },
  //发布服务
  publishService:{
    // 发布服务
    publishSubmit:{
      path:baseUrl+'/front/service/service',
      method:'POST'
    },
    //行业接口
    tradeList:{
      path:baseUrl+'/country/trade/list?tradeCode={tradeCode}',
      method:'GET'
    },
    //获取区域列表
    areaList:{
      path:baseUrl+'/front/area/list?parentId={parentId}&level={level}',
      method:'GET'
    },
    newPublishService:{//新版发布服务
      path:baseUrl + '/housekeeper',
      method:'POST'
    },
    // financeService:{//融资服务
    //   path:baseUrl + '/housekeeper/financing',
    //   method:'POST'
    // },
    // attractService:{//招商服务
    //   path:baseUrl + '/housekeeper/attract',
    //   method:'POST'
    // },
    // projectService:{//项目选址
    //   path:baseUrl + '/housekeeper/location',
    //   method:'POST'
    // },
    // technologyService:{//技术服务
    //   path:baseUrl + '/housekeeper/technology',
    //   method:'POST'
    // },
    // businessService:{//商务服务
    //   path:baseUrl + '/housekeeper/business',
    //   method:'POST'
    // },
    // consultService:{//专家咨询
    //   path:baseUrl + '/housekeeper/consultation',
    //   method:'POST'
    // },
    // meetService:{//邀约会面
    //   path:baseUrl + '/housekeeper/meeting',
    //   method:'POST'
    // },
    // trainService:{//培训招生
    //   path:baseUrl + '/housekeeper/training',
    //   method:'POST'
    // },


  },
  //服务列表
  serviceList:{
    //查看服务分页
    serviceInfo:{
      path:baseUrl+'/front/service/service/page?pageNo={pageNo}&pageSize={pageSize}&serviceMType={serviceMType}',
      method:'GET'
    },
    serviceListInfo:{
      path:baseUrl+'/housekeeper/page?auditStatus={auditStatus}&pageNo={pageNo}&pageSize={pageSize}&type={type}&isRelease={isRelease}&keyWords={keyWords}',
      method:'GET'
    },
    //删除服务
    deleteService:{
      path:baseUrl+'/housekeeper?id={id}&type={type}',
      method:'DELETE'
    },
    //修改服务
    remindService:{
      path:baseUrl+'/housekeeper',
      method:'PUT'
    },
    //查看单个服务
    serviceDetail:{
      path:baseUrl+'/housekeeper?id={id}&type={type}',
      method:'GET'
    },
    servicevisiblity:{//设置服务可见性
      path:baseUrl+'/housekeeper/visibility',
      method:'PUT'
    },
    serviceReload:{//设置发布状态
      path:baseUrl+'/housekeeper/isRelease',
      method:'PUT'
    },
    storeService:{//收藏服务
      path:baseUrl+'/front/collection/housekeeper/page?keyWords={keyWords}&type={type}&pageNo={pageNo}&pageSize={pageSize}',
      method:'GET'
    },
    batchDelete:{//批量删除
      path:baseUrl+'/front/collection?id={id}',
      method:'DELETE'
    },
    serviceDetailInfo:{//查看单个服务
      path:baseUrl+'/front/service/service?id={id}&serviceMType={serviceMType}',
      method:'GET'
    }

  },
  serviceShow:{
    investInfo:{
      path:baseUrl+'/index/housekeeper/page?keyWords={keyWords}&startTime={startTime}&endTime={endTime}&capitalBody={capitalBody}&investmentMode={investmentMode}&type={type}&pageNo={pageNo}&pageSize={pageSize}&amountEnd={amountEnd}&tradeCodes={tradeCodes}&amountStart={amountStart}&areaCodes={areaCodes}',
      method:'GET'
    },
    financeInfo:{
      path:baseUrl+'/index/housekeeper/page?keyWords={keyWords}&startTime={startTime}&endTime={endTime}&financingMode={financingMode}&type={type}&pageNo={pageNo}&pageSize={pageSize}&amountEnd={amountEnd}&tradeCodes={tradeCodes}&amountStart={amountStart}&areaCodes={areaCodes}',
      method:'GET'
    },
    attractInfo:{
      path:baseUrl+'/index/housekeeper/page?keyWords={keyWords}&startTime={startTime}&endTime={endTime}&attractMode={attractMode}&type={type}&pageNo={pageNo}&pageSize={pageSize}&amountEnd={amountEnd}&tradeCodes={tradeCodes}&amountStart={amountStart}&areaCodes={areaCodes}',
      method:'GET'
    },
    selectInfo:{
      path:baseUrl+'/index/housekeeper/page?isRent={isRent}&siteType={siteType}&keyWords={keyWords}&landAreaStart={landAreaStart}&landAreaEnd={landAreaEnd}&type={type}&pageNo={pageNo}&pageSize={pageSize}&areaCodes={areaCodes}',
      method:'GET'
    },
    technologytInfo:{
      path:baseUrl+'/index/housekeeper/page?keyWords={keyWords}&modeSupplement={modeSupplement}&startTime={startTime}&endTime={endTime}&technologyType={technologyType}&technicalStage={technicalStage}&type={type}&pageNo={pageNo}&pageSize={pageSize}&tradeCodes={tradeCodes}&modeCooperation={modeCooperation}',
      method:'GET'
    },
    commercialInfo:{
      path:baseUrl+'/index/housekeeper/page?keyWords={keyWords}&startTime={startTime}&endTime={endTime}&type={type}&pageNo={pageNo}&pageSize={pageSize}',
      method:'GET'
    },
    expertInfo:{
      path:baseUrl+'/index/housekeeper/page?keyWords={keyWords}&startTime={startTime}&endTime={endTime}&type={type}&pageNo={pageNo}&pageSize={pageSize}',
      method:'GET'
    },
    meetInfo:{
      path:baseUrl+'/index/housekeeper/page?keyWords={keyWords}&startTime={startTime}&endTime={endTime}&type={type}&pageNo={pageNo}&pageSize={pageSize}',
      method:'GET'
    },
    trainInfo:{
      path:baseUrl+'/index/housekeeper/page?keyWords={keyWords}&startTime={startTime}&endTime={endTime}&type={type}&pageNo={pageNo}&pageSize={pageSize}',
      method:'GET'
    },
    detailInfo:{//单个服务详情
      path:baseUrl+'/index/housekeeper?id={id}&type={type}&creator={creator}',
      method:'GET'
    },
    // publisherInfo:{//发布人信息
    //   path:baseUrl+'/index/housekeeper/publisher?creator={creator}',
    //   method:'GET'
    // },
    contactInfo:{//联系人信息
      path:baseUrl+'/authc/basic/contacts?id={id}',
      method:'GET'
    },
    // relatedInfo:{//相关推荐信息
    //   path:baseUrl+'/index/housekeeper/page?type={type}&pageSize={pageSize}&pageNo={pageNo}',
    //   method:'GET'
    // },

  },
  //用户资料
  userData:{
    //更新用户资料
    updateUser:{
      path:baseUrl+'/front/user',
      method:'PUT'
    },
    //获取用户信息
    getUserInfo:{
      path:baseUrl+'/userinfo?userId={userId}',
      method:'GET'
    },
    updatePhoneVerify:{//更改手机号校验手机号验证码
      path:baseUrl+'/front/user/phoneVerify',
      method:'POST'
    },
    verifyUpdatePhone:{//修改手机号发送验证码
      path:baseUrl+'/front/updatephone/sms?phone={phone}',
      method:'GET'
    },
    verifyPhone:{//验证码修改密码发送验证码
      path:baseUrl+'/front/updatepwd/sms?phone={phone}',
      method:'GET'
    },
    codeUpdatePwd:{//验证码修改密码
      path:baseUrl+'/front/user/checkPwd/code',
      method:'POST'
    },
    updatePhone:{//修改手机号
      path:baseUrl+'/front/user/phone',
      method:'POST'
    },
    verifyIdentity:{
      path:baseUrl+'/front/authentication',
      method:'PUT'
    },



  },
  //公共接口返回图片前缀及图片上传，头像修改
  commonUrl:{
    base : baseUrl,
    prefix : {
      path:'http://cdn.zhizhaowang.cn/',
    },
    informUrl:{//举报
      path:baseUrl + '/report',
      method:'POST'
    },
    imgUrl:{
      path:baseUrl+'/init/resource',
      method:'GET'
    },
    uploadPic:{
      path:baseUrl + '/upload/pic',
      method:'POST'
    },
    uploadFile:{
      path:baseUrl + '/upload/file',
      method:'POST'
    },
    upLoadMutiple:{
      path:baseUrl+'/upload/file',
      method:'POST'
    },
    headUpLoad:{
      path:baseUrl + '/front/user/headphoto',
      method:'PUT'
    },
    getTrades:{ //获取行业
      path:baseUrl + '/country/trade/list?tradeCode={tradeCode}',
      method:'GET'
    },
    getAreas:{//获取区域
      path:baseUrl + '/front/area/list?parentId={parentId}&level={level}',
      method:'GET'
    },
    vrList:{//获取vr列表
      path:baseUrl + '/country/trade/pano?keyword={keyword}&tradeCode={tradeCode}&type={type}&pageNo={pageNo}&pageSize={pageSize}&areaId={areaId}',
      method:'GET'
    },
    proRelateTradeAndZone:{ //根据行业和区域筛选四种用户列表
      path:baseUrl+'/commercialreaches/user/page?keyword={keyword}&areaId={areaId}&tradeCode={tradeCode}&pageNo={pageNo}&pageSize={pageSize}&type={type}',
      method:'GET'
    },
    detailInfo_company:{//企业详情
      path:baseUrl + '/commercialreaches/company/info?id={id}',
      method:'GET'
    },
    detailInfo_studio:{//个人工作室
      path:baseUrl + '/commercialreaches/studio/info?id={id}',
      method:'GET'
    },
    detailInfo_devzone:{//开发区
      path:baseUrl + '/commercialreaches/devzone/info?id={id}',
      method:'GET'
    },
    detailInfo_merchants:{//招商局
      path:baseUrl + '/commercialreaches/merchants/info?id={id}',
      method:'GET'
    },
    webstructure: {
      path:baseUrl + '/webstructure?id={id}',
      method:'GET'
    },
    addWebForm: {
      path:baseUrl + '/webform',
      method:'POST'
    }
  },
  //修改登录密码
  setPwd:{
    remindPwd:{
      path:baseUrl + '/front/user/updatepass',
      method:'PUT'
    },
    affirmPwd:{
      path:baseUrl + '/front/user/checkpwd',
      method:'POST'
    }
  },
  //区域
  zone:{
    province:{
      path:baseUrl+'/front/area/list?level={level}&parentId={parentId}',
      method:'GET'
    },
    city:{
      path:baseUrl+'/front/area/maps?provinceId={provinceId}',
      method:'GET'
    },
    unit:{
      path:baseUrl+'/front/unit/list?areaId={areaId}&tradeCode={tradeId}',
      method:'GET'
    }
  },
  //新闻
  news:{
    column:{
      path:baseUrl+'/front/column/list',
      method:'GET'
    },
    search:{
      path:baseUrl+'/front/news/page?keyword={keyword}&pageNo={pageNo}&pageSize={pageSize}&isRecommendStr={isRecommendStr}&columnId={columnId}',
      method:'GET'
    },
    detail:{
      path:baseUrl+'/index/news/detail?id={id}',
      method:'GET'
    },
    delete:{
      path:baseUrl+'/front/news?id={id}',
      method:'DELETE'
    },
    edit:{
      path:baseUrl+'/front/news',
      method:'PUT'
    },
    add:{
      path:baseUrl+'/front/news',
      method:'POST'
    }
  },
  //登录接口
  loginManage:{
    loginIn:{
      path:baseUrl+'/user/pwd/login',
      method:'POST'
    },
    loginOut:{
      path:baseUrl+'/user/loginOut',
      method:'GET'
    },
    verifyLoginPhone:{//发送验证码
      path:baseUrl+'/user/sms?phone={phone}',
      method:'GET'
    },
    phoneLogin:{
      path:baseUrl+'/user/code/login?phone={phone}&code={code}',
      method:'GET'
    },
    forgetPwdVerify:{//忘记密码验证码
      path:baseUrl+'/front/updatepwd/sms?phone={phone}',
      method:'GET'
    },
    remindPwd:{//忘记密码新密码设置
      path:baseUrl+'/front/user/forgetpwd',
      method:'POST'
    },
    verifyPwdCode:{//忘记密码校验验证码
      path:baseUrl+'/front/user/forgetpwd/code',
      method:'POST'
    }
  },
  homePage : { //首页接口
    todayNewsHotDetail:{
      path:baseUrl+'/init/headlines?id={id}',
      method:'GET'
    },
    todayNewsHot:{
      path:baseUrl+'/init/headlines/list?maxNum={maxNum}',
      method:'GET'
    },
    newsHotPagination:{
      path:baseUrl+'/init/headlines/page?pageNo={pageNo}&pageSize={pageSize}&keywords={keywords}',
      method:'GET'
    },
    homeNews : {
      path:baseUrl+'/index/news',
      method:'GET'
    },
    column:{
      path:baseUrl+'/index/news/column/page?pageNo={pageNo}&pageSize={pageSize}&columnId={columnId}',
      method:'GET'
    },
    newsDetail : {
      path:baseUrl+'/index/news/detail?id={id}',
      method:'GET'
    },
    wxParams : {
      path:baseUrl+'/index/token?url={url}',
      method:'GET'
    },
    recommendPano:{
      path:baseUrl+'/init/recommend/pano',
      method:'GET'
    },
    xml:{
      path:baseUrl+'/init/getxml'
    },
    vrDetail:{
      path: baseUrl+ '/init/panoinfo?id={id}',
      method:'GET'
    },
    carouselList:{
      path:baseUrl+'/index/focus/list',
      method:'GET'
    }


  },
  panorama:{
    menus:{//老版本已废弃获取栏目菜单树
      path:baseUrl+'/front/pano/panocolumn/list?type={type}&unitId={unitId}&position={position}',
      method:'GET'
    },
    addColumn:{
      path:baseUrl+'/front/pano/panocolumn',
      method:'POST'
    },
  },
  vrColumnManage: {
    columnList: { //VR首页栏目查询
      path: baseUrl + '/pano/panocolumn?security={security}&position={position}',
      method: 'GET'
    },
    getColumnTitle:{
      path: baseUrl + '/pano/panocolumn/bycolumnid?columnId={columnId}&security={security}&pageNo={pageNo}&pageSize={pageSize}',
      method: 'GET'
    },
    getNewContent:{
      path: baseUrl + '/pano/panocolumn/columncontent/byid?id={id}',
      method: 'GET'
    },
    getDetail:{
      path: baseUrl+ '/resource/pano?id={id}',
      method:'GET'
    },
    getList:{
      path: baseUrl+'/pano/panocolumn/bycolumnid?columnId={columnId}&security={security}&&pageNo={pageNo}&pageSize={pageSize}',
      method:'GET'
    },
    getContent:{
      path: baseUrl+'/front/pano/columncontent/byid?id={id}',
      //path: baseUrl+'/pano/panocolumn/columncontent/byid?id={id}',
      method:'GET'
    }

  },
  vrContent:{ //VR栏目内容管理
      update:{
        path:baseUrl+'/front/ftpano/columncontent',
        method:'PUT'
      },
      detail:{
        path:baseUrl+'/front/pano/panocolumn/columncontent?id={id}',
        method:'GET'
      },
      dropDown:{
        path:baseUrl+'/front/pano/panocolumn/columncontent/list?unitId={unitId}&position={position}',
        method:'GET'
      },
      delete:{
        path:baseUrl+'/front/pano/panocolumn/columncontent?id={id}',
        method:'DELETE'
      },
      searchInfo:{
        path:baseUrl+'/front/pano/panocolumn/columncontent/page?queryText={queryText}&columnId={columnId}&unitId={unitId}&pageNo={pageNo}&pageSize={pageSize}',
        method:"GET"
      },
      add:{
        path:baseUrl+'/front/pano/panocolumn/columncontent',
        method:'POST'
      },
      vrClomnDetail:{
        path:baseUrl+'/index/pano/inve/common/list?security={security}&columnName={columnName}',
        method:'GET'
      },
      vrItemDetail:{
        path:baseUrl+'/index/pano/inve/news/page?security={security}&columnName={columnName}&pageSize={pageSize}&pageNo={pageNo}&keywords={keywords}',
        method:'GET'
      }

    },
  vrManage: {
    addCardContent:{ //vrCard内容增加
      path:baseUrl+'/front/ftpano/columncontent',
      method:'POST'
    },
    vrColumnList:{
      path: baseUrl + '/front/ftpano/columncontent/list?type={type}&position={position}',
      method:'GET'
    },
    vrView: {
      path: baseUrl + '/resource/pano/getxml',
      method:'GET'
    },
    vrSet: {
      path: baseUrl + '/resource/pano/setxml'
    },
    vrOder:{//vr排序
      path: baseUrl + '/front/ftpano/column/order',
      method:'PUT'
    },
    vrSingleDetail:{//单个VR查询
      path: baseUrl + '/front/ftpano/column?id={id}',
      method:'GET'
    },
    vrMenus:{//vr栏目菜单树
      path:baseUrl + '/front/ftpano/column/page?pageNo={pageNo}&pageSize={pageSize}&position={position}',
      method:'GET'
    },
    vrDelete:{//删除VR栏目
      path:baseUrl + '/front/ftpano/column?id={id}',
      method:'DELETE'
    },
    AddVrItem:{//新增VR栏目
      path:baseUrl + '/front/ftpano/column',
      method:'POST'
    },
    remindVr:{//修改VR
      path:baseUrl + '/front/ftpano/column',
      method:'PUT'
    },
    onlyVr:{//检测VR唯一性
      path:baseUrl + '/front/ftpano/column/checkColumnName?panoColumnName={panoColumnName}&id={id}',
      method:'GET'
    }
  },
  newVRColContent:{ //新的VR栏目内容管理
    columnPosition:{
      path:baseUrl+'/front/ftpano/columncontent/list?typeStr={typeStr}&position={position}',
      method:'GET'
    },
    update:{
      path:baseUrl+'/front/ftpano/columncontent',
      method:'PUT'
    },
    detail:{
      path:baseUrl+'/front/ftpano/columncontent?id={id}',
      method:'GET'
    },
    dropDown:{
      path:baseUrl+'/front/ftpano/columncontent/column?position={position}',
      method:'GET'
    },
    delete:{ //删除栏目内容
      path:baseUrl+'/front/ftpano/columncontent?id={id}',
      method:'DELETE'
    },
    searchInfo:{
      path:baseUrl+'/front/ftpano/column/columncontent/page?queryText={queryText}&columnId={columnId}&pageNo={pageNo}&pageSize={pageSize}',
      method:"GET"
    },
    add:{
      path:baseUrl+'/front/ftpano/columncontent',
      method:'POST'
    }
  },
  newVRColumn:{//首页全景渲染列表
    columnList:{
      path: baseUrl + '/front/pano/ftpanocolumn?security={security}&position={position}',
      method: 'GET'
    },
    getColumnTitle:{ //得到标题 同时能够得到内容
      path: baseUrl + '/front/pano/columncontent/bycolumnid?columnId={columnId}&security={security}&pageNo={pageNo}&pageSize={pageSize}',
      method: 'GET'
    },
    detail:{
      path: baseUrl + '/front/pano/columncontent/byid?id={id}',
      method: 'GET'
    }
  },
  myVr : {
    info:{
      path: baseUrl + '/resource/pano/userid',
      method: 'GET'
    },
    addVr:{
      path:baseUrl + '/resource/pano',
      method:'POST'
    },
    updateVr : {
      path:baseUrl + '/resource/pano',
      method:'PUT'
    },
    verifyVrStore:{//验证是否收藏
      path:baseUrl+'/front/collection/check?targetId={targetId}',
      method:"GET"
    },
    addVrStore:{//新增VR收藏
      path:baseUrl + '/front/collection',
      method:'POST'
    },
    deleteVrStore:{//删除或取消VR收藏
      path:baseUrl + '/front/collection?id={id}',
      method:'DELETE'
    },
    VrStore:{//vr收藏分页
      path:baseUrl + '/front/collection/vr/page?pageNo={pageNo}&pageSize={pageSize}&type={type}&keywords={keywords}',
      method:'GET'
    },
    panoImgUpload : {
      path : baseUrl + '/resource/pano/upload',
    },
    musicPage : {
      path:baseUrl+'/resource/music/page?keyword={keyword}&pageNo={pageNo}&pageSize={pageSize}',
      method:"GET"
    },
    panoDelete:{
      path:baseUrl + '/resource/pano?panoId={panoId}',
      method:'DELETE'
    },
    addScene : {
      path: baseUrl + '/resource/scene/upload'
    },
    sceneList: {
      path: baseUrl + "/resource/scene/list/groupid?groupId={groupId}",
      method: 'GET'
    },
    editScene : {
      path:baseUrl + '/resource/scene',
      method:'PUT'
    },
    sortScene : {
      path:baseUrl + '/resource/scene/sort',
      method:'PUT'
    },
    deleteScene :{
      path:baseUrl+'/resource/scene?id={id}&panoId={panoId}',
      method:'DELETE'
    }

  },
  panoGroup : {
    add : {
      path : baseUrl + '/resource/scene/group',
      method : 'POST'
    },
    edit : {
      path : baseUrl + '/resource/scene/group',
      method : 'PUT'
    },
    delete : {
      path : baseUrl + '/resource/scene/group?panoId={panoId}&sceneGroupId={sceneGroupId}',
      method : 'DELETE'
    },
    detail : {
      path : baseUrl + '/resource/scene/group?sceneGroupId={sceneGroupId}',
      method: 'GET'
    },
    list : {
      path : baseUrl + '/resource/scene/group/list?panoId={panoId}&sceneGroupId={sceneGroupId}',
      method: 'GET'
    },
    sceneByGroup : {
      path : baseUrl + '/scene/list/groupid?sceneGroupId={groupID}',
      method: 'GET'
    },
    move : {
      path: baseUrl + '/resource/scene/reset/group',
      method: 'PUT'
    }
  },
  music : {
    upload : {
      path : baseUrl + '/resource/music/upload'
    },
    page : {
      path: baseUrl + '/resource/music/page?pageNo={pageNo}&pageSize={pageSize}&keyword={keyword}',
      method: 'GET'
    },
    del : {
      path : baseUrl + '/resource/music?musicId={musicId}',
      method : 'DELETE'
    }
  },
  picture : {
    upload : {
      path : baseUrl + '/resource/pic'
    },
    page : {
      path: baseUrl + '/resource/pic/page?pageNo={pageNo}&pageSize={pageSize}&keyword={keyword}&groupIds={groupIds}',
      method: 'GET'
    },
    edit: {
      path: baseUrl + '/resource/pic',
      method: 'PUT'
    },
    del : {
      path : baseUrl + '/resource/pic?picIds={picIds}',
      method : 'DELETE'
    },
    move : {
      path: baseUrl + '/resource/pic/reset/group',
      method: 'PUT'
    }
  },
  picGroup : {
    list : {
      path: baseUrl + '/resource/pic/group/list?picGroupId={picGroupId}',
      method: 'GET'
    },
    add : {
      path: baseUrl + '/resource/pic/group',
      method: 'POST'
    },
    edit: {
      path: baseUrl + '/resource/pic/group',
      method: 'PUT'
    },
    del : {
      path : baseUrl + '/resource/pic/group?picGroupId={picGroupId}',
      method : 'DELETE'
    },
    detail : {
      path: baseUrl + '/resource/pic/group?picGroupId={picGroupId}',
      method: 'GET'
    }
  },
  ringGroup : {
    list : {
      path: baseUrl + '/resource/ring/group/list?ringGroupId={ringGroupId}',
      method: 'GET'
    },
    add : {
      path: baseUrl + '/resource/ring/group',
      method: 'POST'
    },
    edit: {
      path: baseUrl + '/resource/ring/group',
      method: 'PUT'
    },
    del : {
      path : baseUrl + '/resource/ring/group?ringGroupId={ringGroupId}',
      method : 'DELETE'
    },
    detail : {
      path: baseUrl + '/resource/ring/group?ringGroupId={ringGroupId}',
      method: 'GET'
    }
  },
  ring : {
    upload : {
      path : baseUrl + '/resource/ring/pic/upload'
    },
    page : {
      path: baseUrl + '/resource/ring/page?pageNo={pageNo}&pageSize={pageSize}&keyword={keyword}&groupIds={groupIds}',
      method: 'GET'
    },
    edit: {
      path: baseUrl + '/resource/ring',
      method: 'PUT'
    },
    add: {
      path: baseUrl + '/resource/ring',
      method: 'POST'
    },
    del : {
      path : baseUrl + '/resource/ring?ringId={ringId}',
      method : 'DELETE'
    },
    detail : {
      path: baseUrl + '/resource/ring?ringId={ringId}',
      method: 'GET'
    },
    clearAllPic : {
      path : baseUrl + '/resource/ring/pic?ringId={ringId}',
      method : 'DELETE'
    },
    move : {
      path: baseUrl + '/resource/ring/reset/group',
      method: 'PUT'
    }
  },
  videoGroup : {
    list : {
      path: baseUrl + '/resource/video/group/list?videoGroupId={videoGroupId}',
      method: 'GET'
    },
    add : {
      path: baseUrl + '/resource/video/group',
      method: 'POST'
    },
    edit: {
      path: baseUrl + '/resource/video/group',
      method: 'PUT'
    },
    del : {
      path : baseUrl + '/resource/video/group?videoGroupId={videoGroupId}',
      method : 'DELETE'
    },
    detail : {
      path: baseUrl + '/resource/video/group?videoGroupId={videoGroupId}',
      method: 'GET'
    }
  },
  video : {
    upload : {
      path : baseUrl + '/resource/video/upload'
    },
    page : {
      path: baseUrl + '/resource/video/page?pageNo={pageNo}&pageSize={pageSize}&keyword={keyword}&groupIds={groupIds}',
      method: 'GET'
    },
    edit: {
      path: baseUrl + '/resource/video',
      method: 'PUT'
    },
    add: {
      path: baseUrl + '/resource/video',
      method: 'POST'
    },
    del : {
      path : baseUrl + '/resource/video?videoId={videoId}',
      method : 'DELETE'
    },
    detail : {
      path: baseUrl + '/resource/video?videoId={videoId}',
      method: 'GET'
    },
    clearAllPic : {
      path : baseUrl + '/resource/ring/pic?ringId={ringId}',
      method : 'DELETE'
    },
    move : {
      path: baseUrl + '/resource/video/reset/group',
      method: 'PUT'
    }
  },
  vrColumn5:{
    addColumns:{
      path:baseUrl+'/panorama/column',
      method:'POST'
    },
    initColumn:{
      path:baseUrl+'/panorama/column/list?parentId={parentId}',
      method:'GET'
    },
    checkColumnName:{
      path:baseUrl+'/panorama/column/checkColumnName?panoColumnName={panoColumnName}&id={id}',
      method:'GET'
    },
    deleteColumn:{
      path:baseUrl+'/panorama/column?id={id}',
      method:'DELETE'
    },
    getWholeColumn:{
      path:baseUrl+'/panorama/column/page',
      method:'GET'
    },
    editColumn:{
      path:baseUrl+'/panorama/column',
      method:'PUT'
    },
    editStatus:{
      path:baseUrl+'/panorama/column/ishow',
      method:'PUT'
    },
    orderColumn:{
      path:baseUrl+'/panorama/column/order',
      method:'PUT'
    }
  },
  vrColumn5_con:{
    getColumnList:{
      path:baseUrl+'/panorama/column/list',
      method:'GET'
    },
    addColumnCon:{
      path:baseUrl+'/rama/pano/content',
      method:'POST'
    },
    deleteCard:{
      path:baseUrl+'/rama/pano/content/cards?id={id}',
      method:'DELETE'
    },
    getVrConInfo:{
      path:baseUrl+'/rama/pano/content?id={id}',
      method:'GET'
    },
    editVrConInfo:{
      path:baseUrl+'/rama/pano/content',
      method:'PUT'
    }

  },
  vrColumn5_list:{
    getContentList:{
      path:baseUrl+'/rama/pano/content/page?columnId={columnId}&keyWords={keyWords}&isRelease={isRelease}&startTime={startTime}&endTime={endTime}&pageNo={pageNo}&pageSize={pageSize}',
      method:'GET'
    },
    deleteContent:{
      path:baseUrl+'/rama/pano/content?id={id}',
      method:'DELETE'
    },
    changeRelease:{
      path:baseUrl+'/rama/pano/content/release',
      method:'PUT'
    }
  },
  vrColumn5_show:{
    columnList:{
      path:baseUrl+'/index/pano/rama/menu?creator={creator}',
      method:'GET',
    },
    contentList:{
      path:baseUrl+'/index/pano/rama/content/bycolumnid?columnId={columnId}&pageNo={pageNo}&pageSize={pageSize}',
      method:'GET',
    },
    detailInfo:{ //获取单个内容
      path:baseUrl+'/index/pano/rama/content/byid?id={id}',
      method:'GET',
    },
    judgeCount:{
      path:baseUrl+'/index/pano/rama/content/checkcount?columnId={columnId}',
      method:'GET',
    },
    panoInfo:{
      path:baseUrl+'/index/pano/rama/info?id={id}',
      method:'GET',
    }
  },
  liveVideoList: {
    path: 'http://live.zhizhaowang.cn:9001/front/live/page?pageNo={pageNo}&pageSize={pageSize}&liveName={liveName}',
    method:'GET',
  },
  courseList: {
    path: baseUrl + '/classroom/course/page?pageNo={pageNo}&pageSize={pageSize}&keywords={keywords}',
    method:'GET',
  },

  recommendManage: {
    columnManage: {  // 栏目列表
      path: baseUrl + "/recommend/column/page?pageNo={columnPageNo}&pageSize={columnPageSize}&columnName={columnColumnName}",
      method: "GET"
    },
    alreadyRecommend: {  // 查看已推荐
      path: baseUrl + "/recommend/column/list?pageNo={recommendPageNo}&pageSize={recommendPageSize}&columnName={recommendColumnName}",
      method: "GET"
    },
    alreadyRecommendReplace: {  // 查看已推荐
      path: baseUrl + "/recommend/column/list?pageNo={recommendReplacePageNo}&pageSize={recommendReplacePageSize}&columnName={recommendReplaceColumnName}",
      method: "GET"
    },
    sortUp: {  // 向上排序
      path: baseUrl + "/recommend/column/sort/up",
      method: "PUT"
    },
    sortDown: {  // 向下排序
      path: baseUrl + "/recommend/column/sort/down",
      method: "PUT"
    },
    deleteRecommend: {  // 移出推荐
      path: baseUrl + "/recommend/column?id={id}&orderNum={orderNum}",
      method: "DELETE"
    },
    recommendExceed: {  // 查看推荐数目是否已满
      path: baseUrl + "/recommend/column/exceed",
      method: "GET"
    },
    recommendColumn: {  //
      path: baseUrl + "/recommend/column",
      method: "POST"
    },
    recommendColumnReplace: {  // 查看推荐数目是否已满
      path: baseUrl + "/recommend/column",
      method: "PUT"
    },
    newColumnList: {  // 获取新闻推荐列表
      path: baseUrl + "/recommend/news/wait/page?keyword={columnColumnName}&columnId={columnId}&areaId={areaId}&pageNo={columnPageNo}&pageSize={columnPageSize}",
      method: "GET"
    },
    newAlreadyRecommend: { // 获取已经推荐的新闻
      path: baseUrl + "/recommend/news/page?columnId={columnId}&columnName={recommendColumnName}&pageNo={recommendPageNo}&pageSize={recommendPageSize}",
      method: "GET"
    },
    newsRecommend: {  // 推荐新闻到首页
      path: baseUrl + "/recommend/news",
      method: "POST"
    },
    newsOrder: {  // 查看推荐排序
      path: baseUrl + "/recommend/news/order",
      method: "PUT"
    },
    newContentModification: {  // 新闻内容修改
      path: baseUrl + "/audit/news",
      method: "PUT"
    },
    newsShift: {  // 查看推荐移出
      path: baseUrl + "/recommend/news?id={id}",
      method: "DELETE"
    },
    deleteNwes: { // 批量删除
      path: baseUrl + "/audit/news/list?idStr={idStr}",
      method: "DELETE"
    },
    devzoneList: {   // 开发区推荐/未推荐分页列表
      path: baseUrl + "/recommend/devzone/page?keywords={keywords}&type={type}&pageNo={pageNo}&pageSize={pageSize}",
      method: "GET"
    },
    devzoneRecommend: {   // 开发区推荐
      path: baseUrl + "/recommend/devzone",
      method: "POST"
    },
    devzoneSort: {  //改变已推荐开发区排序情况
      path: baseUrl + "/recommend/devzone",
      method: "PUT"
    },
    devzoneCancel: {  // 开发区取消推荐
      path: baseUrl + "/recommend/devzone?devzoneId={devzoneId}",
      method: "DELETE"
    },
    devzoneChange: {  // 开发区取消推荐
      path: baseUrl + "/recommend/devzone/change",
      method: "PUT"
    },
    newRecommendModification: {  // 新闻推荐状态修改
      path: baseUrl + "/audit/news/recommend",
      method: "PUT"
    },
    newRecommendModificationNew: {  // 更好过后的 新闻推荐状态修改
      path: baseUrl + "/recommend/news",
      method: "PUT"
    },
    carousel: {
      page: {
        path: baseUrl + "/index/focusAll/page?pageNo={pageNo}&pageSize={pageSize}",
        method: "GET"
      },
      detail: {
        path: baseUrl + "/index/focus?id={id}",
        method: "GET"
      },
      add: {
        path: baseUrl + '/index/focus',
        method: 'POST'
      },
      edit: {
        path: baseUrl + '/index/focus',
        method: 'PUT'
      },
      delete: {
        path: baseUrl + '/index/focus?id={id}',
        method: 'DELETE'
      },
      nameValidate: {
        path: baseUrl + '/index/focus/checkName?name={name}&type={type}&id={id}',
        method: 'GET'
      },
      sort: {
        path: baseUrl + '/index/focus/sort?sort={sort}&id={id}',
        method: 'GET'
      }
    },
    localSituation:{
      addContent: { 
        path: baseUrl + "/provincialChannelController/addContent",
        method: "POST"
      },
      upContent: { 
        path: baseUrl + "/provincialChannelController/upContent",
        method: "POST"
      },
      queryContent: { 
        path: baseUrl + "/provincialChannelController/queryContent",
        method: "POST"
      },
      delContent: { 
        path: baseUrl + "/provincialChannelController/delContent",
        method: "POST"
      },
      queryIntention: { 
        path: baseUrl + "/provincialChannelController/queryIntention",
        method: "POST"
      },
      upIntention: { 
        path: baseUrl + "/provincialChannelController/upIntention",
        method: "POST"
      },
      housekeeper: { 
        path: baseUrl + "/index/housekeeper/page?type={type}&pageNo={pageNo}&pageSize={pageSize}&ttType=1",
        method: "GET"
      },

      recommend: { 
        path: baseUrl + "/housekeeper/recommend",
        method: "POST"
      },
    }
  },


  accident: {
    slideshow: {  
      path: baseUrl + "/index/focusAll/page?type=1&areaCode={areaCode}",
      method: "GET"
    },
    queryContent: { 
      path: baseUrl + "/provincialChannelController/queryContent",
      method: "POST"
    },
    project: {  
      path: baseUrl + "/index/housekeeper/page?type=5&ttType=1&pageNo=1&pageSize=5&areaCodes={areaCode}&isRecommend=1",
      method: "GET"
    },
    hannelIndex: {  
      path: baseUrl + "/provincialChannelController/index?areaId={areaId}",
      method: "GET"
    },
  },
}

