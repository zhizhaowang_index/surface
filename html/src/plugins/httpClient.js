//ifSub 是否完成提交 -- <el-button type="primary" :loading="ifSub">加载中</el-button> loading状态
import Vue from 'vue'
const instance = function ($this, subConfig) {
  const self = $this;
  //只有formName存在时进入表单验证，否则直接发起请求
  if (!subConfig.formName) {
    sub_($this, subConfig);
  } else {
    self.$refs[subConfig.formName].validate(function (valid) {
      if (valid) {
        sub_($this, subConfig);
      } else {
        self.$message.error({
          showClose: true,
          message: "内容填写不正确，请重试！"
        });
      }
    });
  }
};
function sub_(self, subConfig) {
  let url = subConfig.url.path;
  //设置地址参数 /{id}
  if (subConfig.params) {
    for (let key in subConfig.params) {
      url = url.replace('{' + key + '}',subConfig.params[key])
    }
  }
  let config = {
    url: url,
    method: subConfig.url.method,
  };

  if (subConfig.url.method === 'GET') {
    config['params'] = subConfig.data
  } else {
    config['data'] = subConfig.data
  }
  Vue.http.request(config).then(data => {

    loadingStatus(self, subConfig, false);
    if (data.code == 200 || data.flag == 200) {
      if (subConfig.callback != undefined) {
        subConfig.callback(data.result);
      }

      if (!(subConfig.noTips!=undefined&&!subConfig.noTips===true)){
        if(data.message){
          self.$message.success({
            showClose: true,
            message: data.message
          });
        }
      }
    }else if(subConfig.error){
      error(subConfig,data.msg);
      return;
    }
    else if(data.code == 1000){
      self.$router.push({path: '/index/login'});
      localStorage.clear();
      self.$store.commit('setCurrentUser',null)
    }else{
      if(data.msg){
        if(!(subConfig.noTips!=undefined&&subConfig.noTips===true)){
          if(data.msg){
            self.$message({
              message:data.msg,
              type:'warning'
            })
          }
        }
      }else if(data.data.error){
        if(!(subConfig.noTips!=undefined&&subConfig.noTips===true)){
          if(data.data.error){
            self.$message({
              message:data.data.error,
              type:'warning'
            })
          }

        }
      }
    }
  }).catch(err => {
    console.log(err)
  });
}

function error(subConfig, err) {
  subConfig.error(err);
}

function loadingStatus(self, subConfig, status) {
  if (subConfig.loading) {
    if (subConfig.loading != "no"){
      //self[subConfig.loading] = status
    }
  } else {
    self.ifSub = status;
  }
}


function plugin(Vue) {
  if (plugin.installed) {
    return;
  }
  Vue.submit = instance;
}

if (typeof  window !== "undefined" && window.Vue) {
  window.Vue.use(plugin);
}

export default plugin;
