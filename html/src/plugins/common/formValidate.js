/**
 * 验证身份证号码
 */
function isIdCardNo(num){
  //　 if (isNaN(num)) {alert("输入的不是数字！"); return false;}
  var len = num.length, re;
  if (len == 15)
    re = new RegExp(/^(\d{6})()?(\d{2})(\d{2})(\d{2})(\d{2})(\w)$/);
  else if (len == 18)
    re = new RegExp(/^(\d{6})()?(\d{4})(\d{2})(\d{2})(\d{3})(\w)$/);
  else {alert("输入的数字位数不对。"); return false;}
  var a = num.match(re);
  if (a != null)
  {
    if (len==15)
    {
      var D = new Date("19"+a[3]+"/"+a[4]+"/"+a[5]);
      var B = D.getYear()==a[3]&&(D.getMonth()+1)==a[4]&&D.getDate()==a[5];
    }
    else
    {
      var D = new Date(a[3]+"/"+a[4]+"/"+a[5]);
      var B = D.getFullYear()==a[3]&&(D.getMonth()+1)==a[4]&&D.getDate()==a[5];
    }
    if (!B) {alert("输入的身份证号 "+ a[0] +" 里出生日期不对。"); return false;}
  }
  if(!re.test(num)){alert("身份证最后一位只能是数字和字母。");return false;}

  return true;
} ;

/**
 * 字符验证，只能包含中文、英文、数字、下划线等字符。
 */
function stringCheck(str){
  if(str==null||str=="") return false;
  var result=str.match(/^[a-zA-Z0-9\u4e00-\u9fa5-_]+$/);
  if(result==null)return false;
  return true;
}

/**
 * 匹配电话号码
 */
function isMobile(str){
  if(str==null||str=="") return false;
  var result=str.match(/^((\(\d{2,3}\))|(\d{3}\-))?((13\d{9})|(15\d{9})|(18\d{9}))$/);
  if(result==null)return false;
  return true;
}

export const validate = {
  isIdCardNo,stringCheck,isMobile
};
