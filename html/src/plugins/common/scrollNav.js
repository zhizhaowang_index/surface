export function scrollNav(){
  return {

    //定位右导航位置 浏览器窗口>=1400
    right_nav: function () {
      $(".user-manual").css("top", $(".protocol").offset().top + 15 + "px")
      $(".user-manual").css("left", $(".protocol").offset().left + $(".protocol").width() + 20 + "px")
      $(".user-manual").show()
    },

    //定位右导航位置  768<浏览器窗口<1400
    right_nav_jotter: function () {
      $(".user-manual").css("top", $(".protocol").offset().top + 8 + "px")
      $(".user-manual").css("left", $(".protocol").offset().left + $(".protocol").width() + 10 + "px")
      $(".user-manual").show()
    },

    //浏览器窗口监控
    window_size: function () {
      orientation.right_nav();
      orientation.right_nav_jotter();

      $(window).resize(function () {
        if ($(window).width() >= 1400) {
          orientation.right_nav();
        } else if (768 < $(window).width() && $(window).width() < 1400) {
          orientation.right_nav_jotter();
        }
      })
    },

    //滚动条监控定位
    scroll_orientation: function () {
      var height = []
      var me = this

      this.heights = function () {
        height = []
        $(".scroll").each(function () {
          height.push($(this).offset().top - 70)
        })
      }

      this.heights()
      $(window).resize(function () {
        me.heights();
      })
      $(window).scroll(function () {
        //console.log(height)
        if ($(document).scrollTop() < height[1]) {
          $(".user-manual li:eq(0)").addClass("manualB").siblings("li").removeClass("manualB")
        } else if (height[1] < $(document).scrollTop() && $(document).scrollTop() < height[2]) {
          $(".user-manual li:eq(1)").addClass("manualB").siblings("li").removeClass("manualB")
        } else if (height[2] < $(document).scrollTop() && $(document).scrollTop() < height[3]) {
          $(".user-manual li:eq(2)").addClass("manualB").siblings("li").removeClass("manualB")
        } else if (height[3] < $(document).scrollTop() && $(document).scrollTop() < height[4]) {
          $(".user-manual li:eq(3)").addClass("manualB").siblings("li").removeClass("manualB")
        } else if (height[4] < $(document).scrollTop()) {
          $(".user-manual li:eq(4)").addClass("manualB").siblings("li").removeClass("manualB")
        }
      })
    },

    //点击定位
    click_orientation: function () {

      var height = []
      var me = this

      this.heights = function () {
        height = []
        $(".scroll").each(function () {
          height.push($(this).offset().top - 69)
        })
      }

      this.heights()
      $(window).resize(function () {
        me.heights();
      })

      $(".user-manual").on("click", "li", function () {
        if (orientation.initclickindex > 0) {
          orientation.scroll_orientation();
        }
        orientation.initclickindex = 1;
        var index = $(this).index()
        $('body,html').animate({scrollTop: height[index]}, 500);
      });
    },

    //点击定位
    click_init: function () {
      $(window.location.hash).addClass('manualB').click();
    }
  }
}
