//import axios from 'axios'
import {Message} from 'element-ui'
const instance = axios.create();
instance.defaults.timeout=10000;
instance.defaults.headers.post['Content-Type'] = 'multipart/form-data; charset=utf-8';

function setCookie(cname,cvalue,exdays){
  var d=new Date();
  d.setTime(d.getTime()+(exdays*24*60*60*1000));
  var expires="expires="+d.toGMTString();
  document.cookie=cname+'='+cvalue+"; "+expires;
};
function getCookie(cname){
  var name=cname+'=';
  var ca=document.cookie.split(";");
  for(var i=0;i<ca.length;i++){
    var c=ca[i].trim();
    if(c.indexOf(name)==0){
      return c.substring(name.length,c.length)
    }
  }
  return "";
};
instance.interceptors.request.use(config => {
  config.withCredentials = true;
  return config;
},error=>{
  Message.error({
    message:'加载超时！'
  });
  return Promise.reject(error)
});

instance.interceptors.response.use(response => {
  return response.data
}, err => {
  return err.response
});

function plugin(Vue) {
  if (plugin.installed) {
    return;
  }
  Vue.http = instance;
}

if (typeof  window !== "undefined" && window.Vue) {
  window.Vue.use(plugin);
}

export default plugin;
